# Danish translation for linuxmint
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-23 20:34+0300\n"
"PO-Revision-Date: 2020-03-10 18:08+0000\n"
"Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>\n"
"Language-Team: Danish <da@li.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-03-13 11:20+0000\n"
"X-Generator: Launchpad (build 3a6db24bbe7280ec09bae73384238390fcc98ad3)\n"

#: live-installer/resources/interface2.ui:66
#: live-installer/resources/interface.ui:131
#, fuzzy
msgid "Welcome to the 17g Installer."
msgstr "Velkommen til installationsprogrammet til 17G."

#: live-installer/resources/interface2.ui:83
#: live-installer/resources/interface.ui:148
#: live-installer/frontend/tui_interface.py:25
#: live-installer/frontend/gtk_interface.py:370
#, fuzzy
msgid ""
"This program will ask you some questions and set up system on your computer."
msgstr ""
"Dette program vil stille dig nogle spørgsmål og installere 17G på din "
"computer."

#: live-installer/resources/interface2.ui:179
#: live-installer/resources/interface2.ui:1437
#: live-installer/resources/interface2.ui:1883
#: live-installer/resources/interface2.ui:1895
#: live-installer/resources/interface2.ui:1907
#: live-installer/resources/interface.ui:244
#: live-installer/resources/interface.ui:1510
#: live-installer/resources/interface.ui:1858
#: live-installer/resources/interface.ui:1870
#: live-installer/resources/interface.ui:1882
msgid "label"
msgstr ""

#: live-installer/resources/interface2.ui:214
#: live-installer/resources/interface.ui:279
msgid "Continent"
msgstr ""

#: live-installer/resources/interface2.ui:228
#: live-installer/resources/interface.ui:293
#: live-installer/frontend/tui_interface.py:41
#: live-installer/frontend/gtk_interface.py:347
msgid "Timezone"
msgstr "Tidszone"

#: live-installer/resources/interface2.ui:271
#: live-installer/resources/interface.ui:338
#: live-installer/frontend/gtk_interface.py:378
msgid "Keyboard Model:"
msgstr "Tastaturmodel:"

#: live-installer/resources/interface2.ui:364
#: live-installer/resources/interface.ui:435
#, fuzzy
msgid "Type here to test your keyboard"
msgstr "Skriv her for at teste dit tastaturlayout"

#: live-installer/resources/interface2.ui:411
#: live-installer/resources/interface.ui:482
#: live-installer/frontend/gtk_interface.py:404
msgid "Automated Installation"
msgstr "Automatiseret installation"

#: live-installer/resources/interface2.ui:427
#: live-installer/resources/interface.ui:498
#, fuzzy
msgid "Erase a disk and install LMDE on it."
msgstr "Slet en disk og installér 17G derpå."

#: live-installer/resources/interface2.ui:455
#: live-installer/resources/interface.ui:526
#: live-installer/frontend/gtk_interface.py:407
msgid "Disk:"
msgstr "Disk:"

#: live-installer/resources/interface2.ui:505
#: live-installer/resources/interface.ui:576
#: live-installer/frontend/gtk_interface.py:415
msgid "Use LVM (Logical Volume Management)"
msgstr "Anvend LVM (Logical Volume Management)"

#: live-installer/resources/interface2.ui:552
#: live-installer/resources/interface.ui:623
#: live-installer/frontend/gtk_interface.py:409
msgid "Encrypt the operating system"
msgstr "Kryptér styresystemet"

#: live-installer/resources/interface2.ui:582
#: live-installer/resources/interface.ui:653
#: live-installer/frontend/gtk_interface.py:411
msgid "Passphrase"
msgstr "Adgangskode"

#: live-installer/resources/interface2.ui:598
#: live-installer/resources/interface.ui:669
#: live-installer/frontend/gtk_interface.py:413
msgid "Confirm passphrase"
msgstr "Bekræft adgangskode"

#: live-installer/resources/interface2.ui:632
#: live-installer/resources/interface.ui:703
#: live-installer/frontend/gtk_interface.py:428
msgid "This provides extra security but it can take hours."
msgstr "Det giver ekstra sikkerhed, men kan tage flere timer."

#: live-installer/resources/interface2.ui:646
#: live-installer/resources/interface.ui:717
#: live-installer/frontend/gtk_interface.py:426
msgid "Fill the disk with random data"
msgstr "Fyld disken med tilfældige data"

#: live-installer/resources/interface2.ui:731
#: live-installer/resources/interface.ui:802
#: live-installer/frontend/gtk_interface.py:417
msgid "Manual Partitioning"
msgstr "Manuel partitionering"

#: live-installer/resources/interface2.ui:747
#: live-installer/resources/interface.ui:818
#, fuzzy
msgid "Manually create, resize or choose partitions for LMDE."
msgstr ""
"Manuel oprettelse, størrelsesændring eller valg af partitioner til 17G."

#: live-installer/resources/interface2.ui:812
#: live-installer/resources/interface.ui:883
msgid "Remove windows & Install"
msgstr ""

#: live-installer/resources/interface2.ui:828
#: live-installer/resources/interface.ui:899
msgid "Remove existsing windows and install LMDE on it."
msgstr ""

#: live-installer/resources/interface2.ui:891
#: live-installer/resources/interface.ui:962
#: live-installer/frontend/gtk_interface.py:451
msgid "Install system with updates"
msgstr ""

#: live-installer/resources/interface2.ui:907
#: live-installer/resources/interface.ui:978
#: live-installer/frontend/gtk_interface.py:452
msgid "If you connect internet, updates will install."
msgstr ""

#: live-installer/resources/interface2.ui:1008
#: live-installer/resources/interface.ui:1079
#: live-installer/frontend/gtk_interface.py:445
msgid "Install the GRUB boot menu on:"
msgstr "Installér GRUB-bootmenuen på:"

#: live-installer/resources/interface2.ui:1066
#: live-installer/resources/interface.ui:1137
#: live-installer/frontend/gtk_interface.py:431
msgid "Edit partitions"
msgstr "Redigér partitioner"

#: live-installer/resources/interface2.ui:1122
#: live-installer/resources/interface.ui:1193
#: live-installer/frontend/gtk_interface.py:383
msgid "Your name:"
msgstr "Dit navn:"

#: live-installer/resources/interface2.ui:1165
#: live-installer/resources/interface.ui:1236
#: live-installer/frontend/gtk_interface.py:385
msgid "Your computer's name:"
msgstr "Navnet på din computer:"

#: live-installer/resources/interface2.ui:1180
#: live-installer/resources/interface.ui:1251
#: live-installer/frontend/gtk_interface.py:389
msgid "Pick a username:"
msgstr "Vælg et brugernavn:"

#: live-installer/resources/interface2.ui:1193
#: live-installer/resources/interface.ui:1264
#: live-installer/frontend/gtk_interface.py:387
msgid "The name it uses when it talks to other computers."
msgstr "Navnet som bruges ved kommunikation med andre computere."

#: live-installer/resources/interface2.ui:1226
#: live-installer/resources/interface.ui:1297
#: live-installer/frontend/gtk_interface.py:391
msgid "Choose a password:"
msgstr "Vælg en adgangskode:"

#: live-installer/resources/interface2.ui:1271
#: live-installer/resources/interface.ui:1342
#: live-installer/frontend/gtk_interface.py:393
msgid "Confirm your password:"
msgstr "Bekræft din adgangskode:"

#: live-installer/resources/interface2.ui:1289
#: live-installer/resources/interface.ui:1360
#: live-installer/frontend/gtk_interface.py:396
msgid "Log in automatically"
msgstr "Log på automatisk"

#: live-installer/resources/interface2.ui:1304
#: live-installer/resources/interface.ui:1375
#: live-installer/frontend/gtk_interface.py:398
msgid "Require my password to log in"
msgstr "Kræv min adgangskode for at logge ind"

#: live-installer/resources/interface2.ui:1319
#: live-installer/resources/interface.ui:1390
#: live-installer/frontend/gtk_interface.py:400
msgid "Encrypt my home folder"
msgstr "Kryptér min Hjem-mappe"

#: live-installer/resources/interface2.ui:1452
#: live-installer/resources/interface.ui:1525
msgid "0.0"
msgstr ""

#: live-installer/resources/interface2.ui:1484
#: live-installer/resources/interface.ui:1557
#: live-installer/frontend/gtk_interface.py:454
msgid "Please do not turn off your computer during the installation process."
msgstr ""

#: live-installer/resources/interface2.ui:1552
#: live-installer/resources/interface.ui:1607
#: live-installer/resources/welcome.ui:206
msgid "17g team"
msgstr ""

#: live-installer/resources/interface2.ui:1755
#: live-installer/resources/interface2.ui:1795
#: live-installer/resources/interface.ui:54
#: live-installer/resources/interface.ui:73
#: live-installer/frontend/gtk_interface.py:343
msgid "Welcome"
msgstr "Velkommen"

#: live-installer/resources/interface2.ui:1919
#: live-installer/resources/interface.ui:1894
msgid "/dev/sda1"
msgstr ""

#: live-installer/resources/welcome.ui:61
msgid "Try 17g"
msgstr ""

#: live-installer/resources/welcome.ui:85
msgid " "
msgstr ""

#: live-installer/resources/welcome.ui:121
#: live-installer/frontend/welcome.py:34
#, fuzzy
msgid "Install to Hard Drive"
msgstr "Installerer drivere"

#: live-installer/resources/welcome.ui:166
msgid "You are currently running 17g from live media."
msgstr ""

#: live-installer/resources/welcome.ui:178
msgid ""
"You can install 17g now, or chose \"Install to Hard Drive\" in the "
"Appication Menu later."
msgstr ""

#: live-installer/resources/welcome.ui:228
#, fuzzy
msgid "Welcome to 17g"
msgstr "Velkommen"

#: live-installer/frontend/partitioning.py:59
msgid "B"
msgstr "B"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:412
msgid "kB"
msgstr "kB"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:412
msgid "MB"
msgstr "MB"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:413
msgid "GB"
msgstr "GB"

#: live-installer/frontend/partitioning.py:60
#: live-installer/frontend/partitioning.py:413
msgid "TB"
msgstr "TB"

#: live-installer/frontend/partitioning.py:67
msgid "Removable:"
msgstr "Flytbar:"

#: live-installer/frontend/partitioning.py:174
msgid "Edit"
msgstr "Redigér"

#: live-installer/frontend/partitioning.py:179
#: live-installer/frontend/partitioning.py:183
#: live-installer/frontend/partitioning.py:187
#: live-installer/frontend/partitioning.py:194
#: live-installer/frontend/partitioning.py:203
#, fuzzy, python-format
msgid "Assign to %s"
msgstr "Tildel til /"

#: live-installer/frontend/partitioning.py:267
msgid "Installation Tool"
msgstr "Installationsværktøj"

#: live-installer/frontend/partitioning.py:268
#, python-format
msgid ""
"No partition table was found on the hard drive: %s. Do you want the "
"installer to create a set of partitions for you? Note: This will ERASE ALL "
"DATA present on this disk."
msgstr ""
"Der blev ikke fundet en partitionstabel på harddisken: %s. Skal "
"installationsprogrammet oprette partitionerne for dig? Bemærk: Dette vil "
"SLETTE ALLE DATA på harddisken."

#: live-installer/frontend/partitioning.py:320
#: live-installer/frontend/partitioning.py:466
#: live-installer/frontend/gtk_interface.py:440
msgid "Free space"
msgstr "Ledig plads"

#: live-installer/frontend/partitioning.py:336
#: live-installer/frontend/gtk_interface.py:331
#: live-installer/frontend/gtk_interface.py:334
#: live-installer/frontend/gtk_interface.py:853
#: live-installer/frontend/gtk_interface.py:889
#: live-installer/frontend/gtk_interface.py:964
#: live-installer/frontend/gtk_interface.py:977
#: live-installer/frontend/gtk_interface.py:982
#: live-installer/frontend/gtk_interface.py:1000
#: live-installer/frontend/gtk_interface.py:1005
#: live-installer/frontend/gtk_interface.py:1011
#: live-installer/frontend/gtk_interface.py:1016
#: live-installer/frontend/gtk_interface.py:1021
#: live-installer/frontend/gtk_interface.py:1075
#: live-installer/frontend/gtk_interface.py:1146
msgid "Installer"
msgstr "Installationsprogram"

#: live-installer/frontend/partitioning.py:352
#, python-format
msgid ""
"The partition table couldn't be written for %s. Restart the computer and try "
"again."
msgstr ""
"Partitionstabellen for %s kunne ikke skrives. Genstart computeren og prøv "
"igen."

#: live-installer/frontend/partitioning.py:399
#, python-format
msgid ""
"The partition %s could not be created. The installation will stop. Restart "
"the computer and try again."
msgstr ""
"Partitionen %s kunne ikke oprettes. Installationen vil stoppe. Genstart "
"computeren og prøv igen."

#: live-installer/frontend/partitioning.py:464
msgid "Logical partition"
msgstr "Logisk partition"

#: live-installer/frontend/partitioning.py:465
msgid "Extended partition"
msgstr "Udvidet partition"

#: live-installer/frontend/partitioning.py:469
msgid "Unknown"
msgstr "Ukendt"

#: live-installer/frontend/partitioning.py:526
msgid "bootloader/recovery"
msgstr ""

#: live-installer/frontend/partitioning.py:538
#, fuzzy
msgid "EFI System Partition"
msgstr "Redigér partition"

#: live-installer/frontend/partitioning.py:561
msgid "Edit partition"
msgstr "Redigér partition"

#: live-installer/frontend/partitioning.py:563
msgid "Device:"
msgstr "Enhed:"

#: live-installer/frontend/partitioning.py:565
msgid "Format as:"
msgstr "Formatér som:"

#: live-installer/frontend/partitioning.py:567
msgid "Mount point:"
msgstr "Monteringspunkt:"

#: live-installer/frontend/partitioning.py:568
msgid "Cancel"
msgstr "Annullér"

#: live-installer/frontend/partitioning.py:569
#: live-installer/frontend/dialogs.py:35 live-installer/frontend/dialogs.py:48
#: live-installer/frontend/dialogs.py:54
msgid "OK"
msgstr "OK"

#: live-installer/frontend/tui_interface.py:23
#: live-installer/frontend/gtk_interface.py:368
#, fuzzy, python-format
msgid "Welcome to the %s Installer."
msgstr "Velkommen til installationsprogrammet til 17G."

#: live-installer/frontend/tui_interface.py:28
#: live-installer/frontend/gtk_interface.py:345
msgid "What language would you like to use?"
msgstr "Hvilket sprog ønsker du at bruge?"

#: live-installer/frontend/tui_interface.py:33
#: live-installer/frontend/gtk_interface.py:106
#: live-installer/frontend/gtk_interface.py:345
#: live-installer/frontend/gtk_interface.py:373
msgid "Language"
msgstr "Sprog"

#: live-installer/frontend/tui_interface.py:37
#: live-installer/frontend/gtk_interface.py:347
msgid "Where are you?"
msgstr "Hvor befinder du dig?"

#: live-installer/frontend/tui_interface.py:45
#, fuzzy
msgid "What keyboard would you like to use?"
msgstr "Hvilket sprog ønsker du at bruge?"

#: live-installer/frontend/tui_interface.py:52
#, fuzzy
msgid "Keyboard Model"
msgstr "Tastaturmodel:"

#: live-installer/frontend/timezones.py:142
#, fuzzy
msgid "Select timezone"
msgstr "Tidszone"

#: live-installer/frontend/dialogs.py:41
msgid "No"
msgstr "Nej"

#: live-installer/frontend/dialogs.py:42
msgid "Yes"
msgstr "Ja"

#: live-installer/frontend/welcome.py:32
#, python-format
msgid "Try %s"
msgstr ""

#: live-installer/frontend/welcome.py:36
#, python-format
msgid "You are currently running %s from live media."
msgstr ""

#: live-installer/frontend/welcome.py:38
#, python-format
msgid ""
"You can install %s now, or chose \"Install to Hard Drive\" in the Appication "
"Menu later."
msgstr ""

#: live-installer/frontend/welcome.py:39
#, fuzzy, python-format
msgid "Welcome to %s"
msgstr "Velkommen"

#: live-installer/frontend/gtk_interface.py:97
#: live-installer/frontend/gtk_interface.py:374
msgid "Country"
msgstr "Land"

#: live-installer/frontend/gtk_interface.py:217
msgid "Layout"
msgstr "Udseende"

#: live-installer/frontend/gtk_interface.py:225
msgid "Variant"
msgstr "Variant"

#: live-installer/frontend/gtk_interface.py:243
msgid "Calculating file indexes ..."
msgstr "Beregner filindekser …"

#: live-installer/frontend/gtk_interface.py:349
msgid "Keyboard layout"
msgstr "Tastaturlayout"

#: live-installer/frontend/gtk_interface.py:349
msgid "What is your keyboard layout?"
msgstr "Hvad er dit tastaturlayout?"

#: live-installer/frontend/gtk_interface.py:351
msgid "User account"
msgstr "Brugerkonto"

#: live-installer/frontend/gtk_interface.py:351
msgid "Who are you?"
msgstr "Hvem er du?"

#: live-installer/frontend/gtk_interface.py:353
msgid "Installation Type"
msgstr "Installationstype"

#: live-installer/frontend/gtk_interface.py:353
#: live-installer/frontend/gtk_interface.py:355
#, fuzzy
msgid "Where do you want to install system?"
msgstr "Hvor vil du installere 17G?"

#: live-installer/frontend/gtk_interface.py:355
msgid "Partitioning"
msgstr "Partitionering"

#: live-installer/frontend/gtk_interface.py:357
msgid "Summary"
msgstr "Oversigt"

#: live-installer/frontend/gtk_interface.py:357
msgid "Check that everything is correct"
msgstr ""

#: live-installer/frontend/gtk_interface.py:359
msgid "Installing"
msgstr "Installerer"

#: live-installer/frontend/gtk_interface.py:359
msgid "Please wait..."
msgstr ""

#: live-installer/frontend/gtk_interface.py:362
msgid "Quit"
msgstr "Afslut"

#: live-installer/frontend/gtk_interface.py:363
msgid "Back"
msgstr "Tilbage"

#: live-installer/frontend/gtk_interface.py:364
#: live-installer/frontend/gtk_interface.py:1158
msgid "Next"
msgstr "Næste"

#: live-installer/frontend/gtk_interface.py:380
msgid "Type here to test your keyboard layout"
msgstr "Skriv her for at teste dit tastaturlayout"

#: live-installer/frontend/gtk_interface.py:406
#, fuzzy
msgid "Erase a disk and install system on it."
msgstr "Slet en disk og installér 17G derpå."

#: live-installer/frontend/gtk_interface.py:419
#, fuzzy
msgid "Manually create, resize or choose partitions for system."
msgstr ""
"Manuel oprettelse, størrelsesændring eller valg af partitioner til 17G."

#: live-installer/frontend/gtk_interface.py:421
msgid "Remove Windows & Install"
msgstr ""

#: live-installer/frontend/gtk_interface.py:423
msgid "Remove existsing windows and install system on it."
msgstr ""

#: live-installer/frontend/gtk_interface.py:432
msgid "Refresh"
msgstr "Opdatér"

#: live-installer/frontend/gtk_interface.py:434
msgid "Device"
msgstr "Enhed"

#: live-installer/frontend/gtk_interface.py:435
msgid "Type"
msgstr "Type"

#: live-installer/frontend/gtk_interface.py:436
msgid "Operating system"
msgstr "Styresystem"

#: live-installer/frontend/gtk_interface.py:437
msgid "Mount point"
msgstr "Monteringspunkt"

#: live-installer/frontend/gtk_interface.py:438
msgid "Format as"
msgstr "Formatér som"

#: live-installer/frontend/gtk_interface.py:439
msgid "Size"
msgstr "Størrelse"

#: live-installer/frontend/gtk_interface.py:597
msgid "Quit?"
msgstr "Afslut?"

#: live-installer/frontend/gtk_interface.py:598
msgid "Are you sure you want to quit the installer?"
msgstr "Er du sikker på, du vil afslutte installationsprogrammet?"

#: live-installer/frontend/gtk_interface.py:854
msgid "Please choose a language"
msgstr "Vælg venligst et sprog"

#: live-installer/frontend/gtk_interface.py:890
#, fuzzy
msgid "Please provide a kayboard layout for your computer."
msgstr "Angiv et navn for din computer."

#: live-installer/frontend/gtk_interface.py:899
msgid "Please provide your full name."
msgstr "Indtast venligst dit fulde navn."

#: live-installer/frontend/gtk_interface.py:904
msgid "Please provide a name for your computer."
msgstr "Angiv et navn for din computer."

#: live-installer/frontend/gtk_interface.py:908
msgid "Please provide a username."
msgstr "Angiv venligst et brugernavn."

#: live-installer/frontend/gtk_interface.py:913
msgid "Please provide a password for your user account."
msgstr "Angiv venligst en adgangskode for din brugerkonto."

#: live-installer/frontend/gtk_interface.py:917
#, fuzzy
msgid "Your passwords is too short."
msgstr "Dine adgangskoder er ikke ens."

#: live-installer/frontend/gtk_interface.py:921
#, fuzzy
msgid "Your passwords is not strong."
msgstr "Dine adgangskoder er ikke ens."

#: live-installer/frontend/gtk_interface.py:925
msgid "Your passwords do not match."
msgstr "Dine adgangskoder er ikke ens."

#: live-installer/frontend/gtk_interface.py:931
#, fuzzy
msgid "Your username cannot start with numbers."
msgstr "Dit brugernavn må ikke indeholde mellemrum."

#: live-installer/frontend/gtk_interface.py:937
msgid "Your username must be lower case."
msgstr "Dit brugernavn skal være med små bogstaver."

#: live-installer/frontend/gtk_interface.py:944
msgid "Your username may not contain whitespace characters."
msgstr "Dit brugernavn må ikke indeholde mellemrum."

#: live-installer/frontend/gtk_interface.py:952
msgid "The computer's name must be lower case."
msgstr "Computerens navn skal være med små bogstaver."

#: live-installer/frontend/gtk_interface.py:959
msgid "The computer's name may not contain whitespace characters."
msgstr "Computerens navn må ikke indeholde blanktegn."

#: live-installer/frontend/gtk_interface.py:978
msgid ""
"Please indicate a filesystem to format the root (/) partition with before "
"proceeding."
msgstr ""
"Vælg venligst et filsystem som rodpartitionen (/) formateres med, før du "
"fortsætter."

#: live-installer/frontend/gtk_interface.py:982
msgid "Please select a root (/) partition."
msgstr "Vælg venligst en rodpartition (/)."

#: live-installer/frontend/gtk_interface.py:983
#, fuzzy, python-format
msgid ""
"A root partition is needed to install %s on.\n"
"\n"
" - Mount point: /\n"
" - Recommended size: 30GB\n"
" - Recommended filesystem format: ext4\n"
"\n"
"Note: The timeshift btrfs snapshots feature requires the use of:\n"
" - subvolume Mount-point /@\n"
" - btrfs as filesystem format\n"
msgstr ""
"En rodpartition er nødvendig til installationen af Linux Mint.\n"
"\n"
" - Monteringspunkt: /\n"
" - Anbefalet størrelse: 30 GB\n"
" - Anbefalet format til filsystemet: ext4\n"
"\n"
"Bemærk: timeshifts funktion til øjebliksbilleder af btrfs kræver følgende:\n"
" - underdiskenheds monteringspunkt /@\n"
" - btrfs  som filsystemformat\n"

#: live-installer/frontend/gtk_interface.py:1001
msgid "The EFI partition is not bootable. Please edit the partition flags."
msgstr ""
"Der kan ikke bootes fra EFI-partitionen. Redigér venligst partitionsflagene."

#: live-installer/frontend/gtk_interface.py:1006
msgid "The EFI partition is too small. It must be at least 35MB."
msgstr "EFI-partitionen er for lille. Den skal mindst være 35 MB."

#: live-installer/frontend/gtk_interface.py:1012
#: live-installer/frontend/gtk_interface.py:1017
msgid "The EFI partition must be formatted as vfat."
msgstr "EFI-partitionen skal formateres som vfat."

#: live-installer/frontend/gtk_interface.py:1021
msgid "Please select an EFI partition."
msgstr "Vælg venligst en EFI-partition."

#: live-installer/frontend/gtk_interface.py:1022
msgid ""
"An EFI system partition is needed with the following requirements:\n"
"\n"
" - Mount point: /boot/efi\n"
" - Partition flags: Bootable\n"
" - Size: at least 35MB (100MB or more recommended)\n"
" - Format: vfat or fat32\n"
"\n"
"To ensure compatibility with Windows we recommend you use the first "
"partition of the disk as the EFI system partition.\n"
" "
msgstr ""
"En EFI-systempartition med de følgende krav er nødvendig:\n"
"\n"
" - Monteringspunkt: /boot/efi\n"
" - Partitionsflag: Bootable\n"
" - Størrelse: mindst 35 MB (100 MB eller mere anbefales)\n"
" - Format: vfat eller fat32\n"
"\n"
"For at sikre kompatibilitet med Windows anbefaler vi, at du bruger diskens "
"første partition som EFI-systempartition.\n"
" "

#: live-installer/frontend/gtk_interface.py:1064
msgid "Please select a disk."
msgstr "Vælg en disk."

#: live-installer/frontend/gtk_interface.py:1070
msgid "Please provide a passphrase for the encryption."
msgstr "Angiv en adgangskode til kryptering."

#: live-installer/frontend/gtk_interface.py:1073
msgid "Your passphrases do not match."
msgstr "Dine adgangskoder stemmer ikke overens."

#: live-installer/frontend/gtk_interface.py:1077
msgid "Warning"
msgstr "Advarsel"

#: live-installer/frontend/gtk_interface.py:1078
#, python-format
msgid "This will delete all the data on %s. Are you sure?"
msgstr "Al data på %s vil blive slettet. Er du sikker?"

#: live-installer/frontend/gtk_interface.py:1139
msgid "Install"
msgstr "Installér"

#: live-installer/frontend/gtk_interface.py:1147
#, fuzzy
msgid "Please provide a device to install grub."
msgstr "Angiv venligst et brugernavn."

#: live-installer/frontend/gtk_interface.py:1186
msgid "Localization"
msgstr "Lokalisering"

#: live-installer/frontend/gtk_interface.py:1187
msgid "Language: "
msgstr "Sprog: "

#: live-installer/frontend/gtk_interface.py:1188
msgid "Timezone: "
msgstr "Tidszone: "

#: live-installer/frontend/gtk_interface.py:1189
msgid "Keyboard layout: "
msgstr "Tastaturlayout: "

#: live-installer/frontend/gtk_interface.py:1193
msgid "User settings"
msgstr "Brugerindstillinger"

#: live-installer/frontend/gtk_interface.py:1194
msgid "Real name: "
msgstr "Rigtige navn: "

#: live-installer/frontend/gtk_interface.py:1195
msgid "Username: "
msgstr "Brugernavn: "

#: live-installer/frontend/gtk_interface.py:1197
msgid "Password: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1199
msgid "Automatic login: "
msgstr "Automatisk login: "

#: live-installer/frontend/gtk_interface.py:1199
#: live-installer/frontend/gtk_interface.py:1202
#: live-installer/frontend/gtk_interface.py:1225
#: live-installer/frontend/gtk_interface.py:1229
msgid "enabled"
msgstr "aktiveret"

#: live-installer/frontend/gtk_interface.py:1200
#: live-installer/frontend/gtk_interface.py:1203
#: live-installer/frontend/gtk_interface.py:1226
#: live-installer/frontend/gtk_interface.py:1229
msgid "disabled"
msgstr "deaktiveret"

#: live-installer/frontend/gtk_interface.py:1202
msgid "Home encryption: "
msgstr "Hjemkryptering: "

#: live-installer/frontend/gtk_interface.py:1204
msgid "System settings"
msgstr "Systemindstillinger"

#: live-installer/frontend/gtk_interface.py:1205
msgid "Computer's name: "
msgstr "Computerens navn: "

#: live-installer/frontend/gtk_interface.py:1208
#: live-installer/frontend/gtk_interface.py:1210
msgid "Bios type: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1212
#, fuzzy
msgid "Install updates after installation"
msgstr "Automatiseret installation"

#: live-installer/frontend/gtk_interface.py:1213
msgid "Filesystem operations"
msgstr "Filsystemhandlinger"

#: live-installer/frontend/gtk_interface.py:1214
#, python-format
msgid "Install bootloader on %s"
msgstr "Installér opstartsindlæser på %s"

#: live-installer/frontend/gtk_interface.py:1215
msgid "Do not install bootloader"
msgstr "Installér ikke opstartsindlæser"

#: live-installer/frontend/gtk_interface.py:1217
msgid "Use already-mounted /target."
msgstr "Anvend det allerede monterede /mål."

#: live-installer/frontend/gtk_interface.py:1222
#, python-format
msgid "Automated installation on %s"
msgstr "Automatiseret installation på %s"

#: live-installer/frontend/gtk_interface.py:1225
msgid "LVM: "
msgstr "LVM: "

#: live-installer/frontend/gtk_interface.py:1228
msgid "Disk Encryption: "
msgstr "Diskkryptering: "

#: live-installer/frontend/gtk_interface.py:1232
#, python-format
msgid "Format %(path)s as %(filesystem)s"
msgstr "Formatér %(path)s som %(filesystem)s"

#: live-installer/frontend/gtk_interface.py:1236
#, python-format
msgid "Mount %(path)s as %(mount)s"
msgstr "Montér %(path)s som %(mount)s"

#: live-installer/frontend/gtk_interface.py:1248
#: live-installer/installer.py:821
msgid "Installation finished"
msgstr "Installationen er fuldført"

#: live-installer/frontend/gtk_interface.py:1249
msgid ""
"The installation is now complete. Do you want to restart your computer to "
"use the new system?"
msgstr ""
"Installationen er nu færdig. Vil du genstarte din computer og tage det nye "
"system i brug?"

#: live-installer/frontend/gtk_interface.py:1280
#: live-installer/frontend/gtk_interface.py:1284
#: live-installer/frontend/gtk_interface.py:1293
#: live-installer/frontend/gtk_interface.py:1302
msgid "Installation error"
msgstr "Installationsfejl"

#: live-installer/main.py:34
msgid "You must be root!"
msgstr ""

#: live-installer/installer.py:133 live-installer/installer.py:154
#, python-format
msgid "Copying /%s"
msgstr "Kopierer /%s"

#: live-installer/installer.py:134
#, python-format
msgid "rsync exited with return code: %s"
msgstr ""

#: live-installer/installer.py:138
msgid "Extracting rootfs."
msgstr ""

#: live-installer/installer.py:174
msgid "Entering the system ..."
msgstr "Går ind i systemet …"

#: live-installer/installer.py:204
msgid "Adding new user to the system"
msgstr "Tilføjer ny bruger til systemet"

#: live-installer/installer.py:255
msgid "Writing filesystem mount information to /etc/fstab"
msgstr "Skriver information om filsystemets montering i /etc/fstab"

#: live-installer/installer.py:261 live-installer/installer.py:417
#: live-installer/installer.py:432
#, python-format
msgid "Mounting %(partition)s on %(mountpoint)s"
msgstr "Monterer %(partition)s på %(mountpoint)s"

#: live-installer/installer.py:298
#, python-format
msgid "Filling %s with random data (please be patient, this can take hours...)"
msgstr ""
"Fylder %s med tilfældige data (hav tålmodighed, det kan godt tage flere "
"timer)"

#: live-installer/installer.py:304
#, python-format
msgid "Creating partitions on %s"
msgstr "Opretter partitioner på %s"

#: live-installer/installer.py:369
#, python-format
msgid "Formatting %(partition)s as %(format)s ..."
msgstr "Formaterer %(partition)s som %(format)s …"

#: live-installer/installer.py:558
msgid "Setting hostname"
msgstr "Sætter værtsnavn op"

#: live-installer/installer.py:583
msgid "Setting locale"
msgstr "Sætter regionsinformation op"

#: live-installer/installer.py:604
#, fuzzy
msgid "Setting timezone"
msgstr "Sætter værtsnavn op"

#: live-installer/installer.py:637
msgid "Setting keyboard options"
msgstr "Sætter tastaturindstillinger op"

#: live-installer/installer.py:733
msgid "Trying to install updates"
msgstr ""

#: live-installer/installer.py:737
msgid "Clearing package manager"
msgstr ""

#: live-installer/installer.py:754
msgid "Generating initramfs"
msgstr ""

#: live-installer/installer.py:759
msgid "Preparing bootloader installation"
msgstr ""

#: live-installer/installer.py:771
msgid "Installing bootloader"
msgstr "Installerer opstartsindlæser"

#: live-installer/installer.py:783
msgid "Configuring bootloader"
msgstr "Konfigurerer opstartsindlæseren"

#: live-installer/installer.py:791
msgid ""
"WARNING: The grub bootloader was not configured properly! You need to "
"configure it manually."
msgstr ""
"ADVARSEL: Grub-opstartsindlæseren blev ikke konfigureret ordentligt! Du er "
"nødt til at foretage konfigurationen manuelt."

#: live-installer/installer.py:844
msgid "Checking bootloader"
msgstr "Tjekker opstartsindlæseren"

#: live-installer/installer.py:876
msgid "Failed to run command (Exited with {}):"
msgstr ""

#, fuzzy
#~ msgid "Select additional options"
#~ msgstr "Sætter tastaturindstillinger op"

#~ msgid ""
#~ "* Your username, your computer's name and your password should only "
#~ "contain Latin characters. In addition to your selected layout, English "
#~ "(US) is set as the default. You can switch layouts by pressing both Ctrl "
#~ "keys together."
#~ msgstr ""
#~ "* Dit brugernavn, din computers navn og din adgangskode må kun indeholde "
#~ "latinske bogstaver. Udover det valgte layout er Engelsk (US) sat som "
#~ "standard. Du kan skifte layout ved at trykke på begge Ctrl-taster "
#~ "samtidig."

#~ msgid ""
#~ "ERROR: You must first manually mount your target filesystem(s) at /target "
#~ "to do a custom install!"
#~ msgstr ""
#~ "FEJL: Du skal første montere dine målfilsystemer på /mål for at lave en "
#~ "brugerdefineret installation!"

#~ msgid "Removing live configuration (packages)"
#~ msgstr "Fjerner live-opsætningen (pakker)"

#~ msgid "ERROR: the use of @subvolumes is limited to btrfs"
#~ msgstr "FEJL: Brugen af @underdiskenheder er begrænset til btrfs"

#~ msgid "Localizing packages"
#~ msgstr "Lokalisering af pakker"

#~ msgid "Cleaning APT"
#~ msgstr "Renser APT"

#~ msgid "Advanced options"
#~ msgstr "Avancerede indstillinger"

#~ msgid "Expert mode"
#~ msgstr "Eksperttilstand"

#~ msgid "Installation paused"
#~ msgstr "Installationen er sat på pause"

#~ msgid ""
#~ "You selected to manage your partitions manually, this feature is for "
#~ "ADVANCED USERS ONLY."
#~ msgstr ""
#~ "Du har valgt at håndtere dine partitioner selv. Denne funktion er kun for "
#~ "øvede brugere."

#~ msgid "Before continuing, mount your target filesystem(s) on /target."
#~ msgstr "Montér dine målfilsystemer på /mål, før du fortsætter"

#~ msgid ""
#~ "Do NOT mount virtual devices such as /dev, /proc, /sys, etc on /target/."
#~ msgstr ""
#~ "Montér ikke virtuelle enheder såsom /dev, /proc, /sys osv. på /mål/."

#~ msgid ""
#~ "During the install, you will be given time to chroot into /target and "
#~ "install any packages that will be needed to boot your new system."
#~ msgstr ""
#~ "Under installationen vil du få lejlighed til at \"chroot\" ind i /mål og "
#~ "installere pakker, som er påkrævede for at boote dit nye system."

#~ msgid ""
#~ "During the install, you will be required to write your own /etc/fstab."
#~ msgstr "Under installationen skal du lave din egen /etc/fstab."

#~ msgid "Do the following and then click Next to finish installation:"
#~ msgstr "Gør følgende og klik Næste for at færdiggøre installationen:"

#~ msgid ""
#~ "Create /target/etc/fstab for the filesystems as they will be mounted in "
#~ "your new system, matching those currently mounted at /target (without "
#~ "using the /target prefix in the mount paths themselves)."
#~ msgstr ""
#~ "Opret /mål/etc/fstab for filsystemerne, som de vil blive monteret i dit "
#~ "nye system. Filsystemerne vil matche dem, som for tiden er monterede på /"
#~ "mål (uden at anvende /mål som præfiks i monteringsstierne)."

#~ msgid ""
#~ "Install any packages that may be needed for first boot (mdadm, "
#~ "cryptsetup, dmraid, etc) by calling \"sudo chroot /target\" followed by "
#~ "the relevant apt-get/aptitude installations."
#~ msgstr ""
#~ "Installér pakker, som kan være nødvendige for den første opstart (mdadm, "
#~ "cryptsetup, dmraid osv.) ved at køre \"sudo chroot /mål\" efterfulgt af "
#~ "de relevante installeringer med apt-get/aptitude."

#~ msgid ""
#~ "Note that in order for update-initramfs to work properly in some cases "
#~ "(such as dm-crypt), you may need to have drives currently mounted using "
#~ "the same block device name as they appear in /target/etc/fstab."
#~ msgstr ""
#~ "I nogle tilfælde (f.eks. dm-crypt) kan det være nødvendigt at have drev "
#~ "monterede med de samme blokenhedsnavne som de optræder med i /mål/etc/"
#~ "fstab for at få update-initramfs til at fungere ordentligt."

#~ msgid ""
#~ "Double-check that your /target/etc/fstab is correct, matches what your "
#~ "new system will have at first boot, and matches what is currently mounted "
#~ "at /target."
#~ msgstr ""
#~ "Dobbelttjek at din /mål/etc/fstab er korrekt, matcher hvad dit nye system "
#~ "vil have under den første opstart og hvad der lige nu er monteret på /mål."

#~ msgid "A root subvolume (/@) requires to format the partition with btrfs."
#~ msgstr ""
#~ "En rodunderdiskenhed (/@) er nødvendig for at formatere partitionen med "
#~ "btrfs."

#~ msgid ""
#~ "A home subvolume (/@home) requires the use of a btrfs formatted partition."
#~ msgstr ""
#~ "En hjemunderdiskenhed (/@home) kræver brugen af en partition formateret "
#~ "med btrfs."

#~ msgid ""
#~ "The installation is now paused. Please read the instructions on the page "
#~ "carefully before clicking Next to finish the installation."
#~ msgstr ""
#~ "Installationen er sat på pause. Læs instruktionerne på siden omhyggeligt, "
#~ "før du klikker Næste og færdiggør installationen."

#~ msgid "Assign to /home"
#~ msgstr "Tildel til /home"

#~ msgid "Assign to /boot/efi"
#~ msgstr "Tildel til /boot/efi"
