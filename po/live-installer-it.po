# Italian translation for linuxmint
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-23 20:34+0300\n"
"PO-Revision-Date: 2020-03-11 17:22+0000\n"
"Last-Translator: Dragone2 <Unknown>\n"
"Language-Team: Italian <it@li.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-03-13 11:20+0000\n"
"X-Generator: Launchpad (build 3a6db24bbe7280ec09bae73384238390fcc98ad3)\n"

#: live-installer/resources/interface2.ui:66
#: live-installer/resources/interface.ui:131
#, fuzzy
msgid "Welcome to the 17g Installer."
msgstr "Benvenuto/a nell'installazione di 17G"

#: live-installer/resources/interface2.ui:83
#: live-installer/resources/interface.ui:148
#: live-installer/frontend/tui_interface.py:25
#: live-installer/frontend/gtk_interface.py:370
#, fuzzy
msgid ""
"This program will ask you some questions and set up system on your computer."
msgstr ""
"Questo programma ti farà alcune domande e configurerà 17G sul tuo computer."

#: live-installer/resources/interface2.ui:179
#: live-installer/resources/interface2.ui:1437
#: live-installer/resources/interface2.ui:1883
#: live-installer/resources/interface2.ui:1895
#: live-installer/resources/interface2.ui:1907
#: live-installer/resources/interface.ui:244
#: live-installer/resources/interface.ui:1510
#: live-installer/resources/interface.ui:1858
#: live-installer/resources/interface.ui:1870
#: live-installer/resources/interface.ui:1882
msgid "label"
msgstr ""

#: live-installer/resources/interface2.ui:214
#: live-installer/resources/interface.ui:279
msgid "Continent"
msgstr ""

#: live-installer/resources/interface2.ui:228
#: live-installer/resources/interface.ui:293
#: live-installer/frontend/tui_interface.py:41
#: live-installer/frontend/gtk_interface.py:347
msgid "Timezone"
msgstr "Fuso orario"

#: live-installer/resources/interface2.ui:271
#: live-installer/resources/interface.ui:338
#: live-installer/frontend/gtk_interface.py:378
msgid "Keyboard Model:"
msgstr "Modello Tastiera:"

#: live-installer/resources/interface2.ui:364
#: live-installer/resources/interface.ui:435
#, fuzzy
msgid "Type here to test your keyboard"
msgstr "Digita qui per testare il layout della tua tastiera"

#: live-installer/resources/interface2.ui:411
#: live-installer/resources/interface.ui:482
#: live-installer/frontend/gtk_interface.py:404
msgid "Automated Installation"
msgstr "Installazione Automatica"

#: live-installer/resources/interface2.ui:427
#: live-installer/resources/interface.ui:498
#, fuzzy
msgid "Erase a disk and install LMDE on it."
msgstr "Cancella un disco e installa 17G su di esso."

#: live-installer/resources/interface2.ui:455
#: live-installer/resources/interface.ui:526
#: live-installer/frontend/gtk_interface.py:407
msgid "Disk:"
msgstr "Disco:"

#: live-installer/resources/interface2.ui:505
#: live-installer/resources/interface.ui:576
#: live-installer/frontend/gtk_interface.py:415
msgid "Use LVM (Logical Volume Management)"
msgstr "Usa LVM (Logical Volume Management)"

#: live-installer/resources/interface2.ui:552
#: live-installer/resources/interface.ui:623
#: live-installer/frontend/gtk_interface.py:409
msgid "Encrypt the operating system"
msgstr "Crittografa il sistema operativo"

#: live-installer/resources/interface2.ui:582
#: live-installer/resources/interface.ui:653
#: live-installer/frontend/gtk_interface.py:411
msgid "Passphrase"
msgstr "Frase di accesso"

#: live-installer/resources/interface2.ui:598
#: live-installer/resources/interface.ui:669
#: live-installer/frontend/gtk_interface.py:413
msgid "Confirm passphrase"
msgstr "Conferma la frase di accesso"

#: live-installer/resources/interface2.ui:632
#: live-installer/resources/interface.ui:703
#: live-installer/frontend/gtk_interface.py:428
msgid "This provides extra security but it can take hours."
msgstr "Questo garantisce una maggiore sicurezza, ma può richiedere ore."

#: live-installer/resources/interface2.ui:646
#: live-installer/resources/interface.ui:717
#: live-installer/frontend/gtk_interface.py:426
msgid "Fill the disk with random data"
msgstr "Riempi il disco con dati casuali"

#: live-installer/resources/interface2.ui:731
#: live-installer/resources/interface.ui:802
#: live-installer/frontend/gtk_interface.py:417
msgid "Manual Partitioning"
msgstr "Partizionamento Manuale"

#: live-installer/resources/interface2.ui:747
#: live-installer/resources/interface.ui:818
#, fuzzy
msgid "Manually create, resize or choose partitions for LMDE."
msgstr "Crea, ridimensiona o scegli manualmente le partizioni per 17G."

#: live-installer/resources/interface2.ui:812
#: live-installer/resources/interface.ui:883
msgid "Remove windows & Install"
msgstr ""

#: live-installer/resources/interface2.ui:828
#: live-installer/resources/interface.ui:899
msgid "Remove existsing windows and install LMDE on it."
msgstr ""

#: live-installer/resources/interface2.ui:891
#: live-installer/resources/interface.ui:962
#: live-installer/frontend/gtk_interface.py:451
msgid "Install system with updates"
msgstr ""

#: live-installer/resources/interface2.ui:907
#: live-installer/resources/interface.ui:978
#: live-installer/frontend/gtk_interface.py:452
msgid "If you connect internet, updates will install."
msgstr ""

#: live-installer/resources/interface2.ui:1008
#: live-installer/resources/interface.ui:1079
#: live-installer/frontend/gtk_interface.py:445
msgid "Install the GRUB boot menu on:"
msgstr "Installa il menù di avvio GRUB su:"

#: live-installer/resources/interface2.ui:1066
#: live-installer/resources/interface.ui:1137
#: live-installer/frontend/gtk_interface.py:431
msgid "Edit partitions"
msgstr "Modifica partizioni"

#: live-installer/resources/interface2.ui:1122
#: live-installer/resources/interface.ui:1193
#: live-installer/frontend/gtk_interface.py:383
msgid "Your name:"
msgstr "Il tuo nome:"

#: live-installer/resources/interface2.ui:1165
#: live-installer/resources/interface.ui:1236
#: live-installer/frontend/gtk_interface.py:385
msgid "Your computer's name:"
msgstr "Il nome del tuo computer:"

#: live-installer/resources/interface2.ui:1180
#: live-installer/resources/interface.ui:1251
#: live-installer/frontend/gtk_interface.py:389
msgid "Pick a username:"
msgstr "Scegli un nome utente:"

#: live-installer/resources/interface2.ui:1193
#: live-installer/resources/interface.ui:1264
#: live-installer/frontend/gtk_interface.py:387
msgid "The name it uses when it talks to other computers."
msgstr "Il nome utilizzato per essere identificato da altri computer."

#: live-installer/resources/interface2.ui:1226
#: live-installer/resources/interface.ui:1297
#: live-installer/frontend/gtk_interface.py:391
msgid "Choose a password:"
msgstr "Scegli una password:"

#: live-installer/resources/interface2.ui:1271
#: live-installer/resources/interface.ui:1342
#: live-installer/frontend/gtk_interface.py:393
msgid "Confirm your password:"
msgstr "Conferma la tua password:"

#: live-installer/resources/interface2.ui:1289
#: live-installer/resources/interface.ui:1360
#: live-installer/frontend/gtk_interface.py:396
msgid "Log in automatically"
msgstr "Accesso automatico"

#: live-installer/resources/interface2.ui:1304
#: live-installer/resources/interface.ui:1375
#: live-installer/frontend/gtk_interface.py:398
msgid "Require my password to log in"
msgstr "Richiedi la mia password all'accesso"

#: live-installer/resources/interface2.ui:1319
#: live-installer/resources/interface.ui:1390
#: live-installer/frontend/gtk_interface.py:400
msgid "Encrypt my home folder"
msgstr "Cifra la mia cartella personale"

#: live-installer/resources/interface2.ui:1452
#: live-installer/resources/interface.ui:1525
msgid "0.0"
msgstr ""

#: live-installer/resources/interface2.ui:1484
#: live-installer/resources/interface.ui:1557
#: live-installer/frontend/gtk_interface.py:454
msgid "Please do not turn off your computer during the installation process."
msgstr ""

#: live-installer/resources/interface2.ui:1552
#: live-installer/resources/interface.ui:1607
#: live-installer/resources/welcome.ui:206
msgid "17g team"
msgstr ""

#: live-installer/resources/interface2.ui:1755
#: live-installer/resources/interface2.ui:1795
#: live-installer/resources/interface.ui:54
#: live-installer/resources/interface.ui:73
#: live-installer/frontend/gtk_interface.py:343
msgid "Welcome"
msgstr "Benvenuto/a"

#: live-installer/resources/interface2.ui:1919
#: live-installer/resources/interface.ui:1894
msgid "/dev/sda1"
msgstr ""

#: live-installer/resources/welcome.ui:61
msgid "Try 17g"
msgstr ""

#: live-installer/resources/welcome.ui:85
msgid " "
msgstr ""

#: live-installer/resources/welcome.ui:121
#: live-installer/frontend/welcome.py:34
#, fuzzy
msgid "Install to Hard Drive"
msgstr "Installazione dei driver"

#: live-installer/resources/welcome.ui:166
msgid "You are currently running 17g from live media."
msgstr ""

#: live-installer/resources/welcome.ui:178
msgid ""
"You can install 17g now, or chose \"Install to Hard Drive\" in the "
"Appication Menu later."
msgstr ""

#: live-installer/resources/welcome.ui:228
#, fuzzy
msgid "Welcome to 17g"
msgstr "Benvenuto/a"

#: live-installer/frontend/partitioning.py:59
msgid "B"
msgstr "B"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:412
msgid "kB"
msgstr "KB"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:412
msgid "MB"
msgstr "MB"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:413
msgid "GB"
msgstr "GB"

#: live-installer/frontend/partitioning.py:60
#: live-installer/frontend/partitioning.py:413
msgid "TB"
msgstr "TB"

#: live-installer/frontend/partitioning.py:67
msgid "Removable:"
msgstr "Rimuovibile:"

#: live-installer/frontend/partitioning.py:174
msgid "Edit"
msgstr "Modifica"

#: live-installer/frontend/partitioning.py:179
#: live-installer/frontend/partitioning.py:183
#: live-installer/frontend/partitioning.py:187
#: live-installer/frontend/partitioning.py:194
#: live-installer/frontend/partitioning.py:203
#, fuzzy, python-format
msgid "Assign to %s"
msgstr "Assegnare a /"

#: live-installer/frontend/partitioning.py:267
msgid "Installation Tool"
msgstr "Strumento di installazione"

#: live-installer/frontend/partitioning.py:268
#, python-format
msgid ""
"No partition table was found on the hard drive: %s. Do you want the "
"installer to create a set of partitions for you? Note: This will ERASE ALL "
"DATA present on this disk."
msgstr ""
"Impossibile trovare una tavola di partizioni nel disco: %s. Vuoi che il "
"programma d'installazione si occupi della creazione delle partizioni per te? "
"Attenzione: Questa operazione CANCELLERÀ TUTTI I DATI presenti in questo "
"disco."

#: live-installer/frontend/partitioning.py:320
#: live-installer/frontend/partitioning.py:466
#: live-installer/frontend/gtk_interface.py:440
msgid "Free space"
msgstr "Spazio libero"

#: live-installer/frontend/partitioning.py:336
#: live-installer/frontend/gtk_interface.py:331
#: live-installer/frontend/gtk_interface.py:334
#: live-installer/frontend/gtk_interface.py:853
#: live-installer/frontend/gtk_interface.py:889
#: live-installer/frontend/gtk_interface.py:964
#: live-installer/frontend/gtk_interface.py:977
#: live-installer/frontend/gtk_interface.py:982
#: live-installer/frontend/gtk_interface.py:1000
#: live-installer/frontend/gtk_interface.py:1005
#: live-installer/frontend/gtk_interface.py:1011
#: live-installer/frontend/gtk_interface.py:1016
#: live-installer/frontend/gtk_interface.py:1021
#: live-installer/frontend/gtk_interface.py:1075
#: live-installer/frontend/gtk_interface.py:1146
msgid "Installer"
msgstr "Programma di Installazione"

#: live-installer/frontend/partitioning.py:352
#, python-format
msgid ""
"The partition table couldn't be written for %s. Restart the computer and try "
"again."
msgstr ""
"Non è stato possibile scrivere la tabella delle partizioni per %s. Riavviare "
"il computer e riprovare."

#: live-installer/frontend/partitioning.py:399
#, python-format
msgid ""
"The partition %s could not be created. The installation will stop. Restart "
"the computer and try again."
msgstr ""
"Non è stato possibile creare la partizione %s. L'installazione si fermerà. "
"Riavviare il computer e riprovare."

#: live-installer/frontend/partitioning.py:464
msgid "Logical partition"
msgstr "Partizione logica"

#: live-installer/frontend/partitioning.py:465
msgid "Extended partition"
msgstr "Partizione estesa"

#: live-installer/frontend/partitioning.py:469
msgid "Unknown"
msgstr "Sconosciuta"

#: live-installer/frontend/partitioning.py:526
msgid "bootloader/recovery"
msgstr ""

#: live-installer/frontend/partitioning.py:538
#, fuzzy
msgid "EFI System Partition"
msgstr "Modifica partizione"

#: live-installer/frontend/partitioning.py:561
msgid "Edit partition"
msgstr "Modifica partizione"

#: live-installer/frontend/partitioning.py:563
msgid "Device:"
msgstr "Dispositivo:"

#: live-installer/frontend/partitioning.py:565
msgid "Format as:"
msgstr "Formattare come:"

#: live-installer/frontend/partitioning.py:567
msgid "Mount point:"
msgstr "Punto di montaggio:"

#: live-installer/frontend/partitioning.py:568
msgid "Cancel"
msgstr "Annulla"

#: live-installer/frontend/partitioning.py:569
#: live-installer/frontend/dialogs.py:35 live-installer/frontend/dialogs.py:48
#: live-installer/frontend/dialogs.py:54
msgid "OK"
msgstr "OK"

#: live-installer/frontend/tui_interface.py:23
#: live-installer/frontend/gtk_interface.py:368
#, fuzzy, python-format
msgid "Welcome to the %s Installer."
msgstr "Benvenuto/a nell'installazione di 17G"

#: live-installer/frontend/tui_interface.py:28
#: live-installer/frontend/gtk_interface.py:345
msgid "What language would you like to use?"
msgstr "Quale lingua vorresti utilizzare?"

#: live-installer/frontend/tui_interface.py:33
#: live-installer/frontend/gtk_interface.py:106
#: live-installer/frontend/gtk_interface.py:345
#: live-installer/frontend/gtk_interface.py:373
msgid "Language"
msgstr "Lingua"

#: live-installer/frontend/tui_interface.py:37
#: live-installer/frontend/gtk_interface.py:347
msgid "Where are you?"
msgstr "Dove sei?"

#: live-installer/frontend/tui_interface.py:45
#, fuzzy
msgid "What keyboard would you like to use?"
msgstr "Quale lingua vorresti utilizzare?"

#: live-installer/frontend/tui_interface.py:52
#, fuzzy
msgid "Keyboard Model"
msgstr "Modello Tastiera:"

#: live-installer/frontend/timezones.py:142
#, fuzzy
msgid "Select timezone"
msgstr "Fuso orario"

#: live-installer/frontend/dialogs.py:41
msgid "No"
msgstr "No"

#: live-installer/frontend/dialogs.py:42
msgid "Yes"
msgstr "Sì"

#: live-installer/frontend/welcome.py:32
#, python-format
msgid "Try %s"
msgstr ""

#: live-installer/frontend/welcome.py:36
#, python-format
msgid "You are currently running %s from live media."
msgstr ""

#: live-installer/frontend/welcome.py:38
#, python-format
msgid ""
"You can install %s now, or chose \"Install to Hard Drive\" in the Appication "
"Menu later."
msgstr ""

#: live-installer/frontend/welcome.py:39
#, fuzzy, python-format
msgid "Welcome to %s"
msgstr "Benvenuto/a"

#: live-installer/frontend/gtk_interface.py:97
#: live-installer/frontend/gtk_interface.py:374
msgid "Country"
msgstr "Paese"

#: live-installer/frontend/gtk_interface.py:217
msgid "Layout"
msgstr "configurazione"

#: live-installer/frontend/gtk_interface.py:225
msgid "Variant"
msgstr "Variante"

#: live-installer/frontend/gtk_interface.py:243
msgid "Calculating file indexes ..."
msgstr "Calcolo indici dei file ..."

#: live-installer/frontend/gtk_interface.py:349
msgid "Keyboard layout"
msgstr "Disposizione della tastiera"

#: live-installer/frontend/gtk_interface.py:349
msgid "What is your keyboard layout?"
msgstr "Qual è il layout della tua tastiera?"

#: live-installer/frontend/gtk_interface.py:351
msgid "User account"
msgstr "Account utente"

#: live-installer/frontend/gtk_interface.py:351
msgid "Who are you?"
msgstr "Chi sei?"

#: live-installer/frontend/gtk_interface.py:353
msgid "Installation Type"
msgstr "Tipologia di Installazione"

#: live-installer/frontend/gtk_interface.py:353
#: live-installer/frontend/gtk_interface.py:355
#, fuzzy
msgid "Where do you want to install system?"
msgstr "Dove desideri installare 17G?"

#: live-installer/frontend/gtk_interface.py:355
msgid "Partitioning"
msgstr "Partizionamento"

#: live-installer/frontend/gtk_interface.py:357
msgid "Summary"
msgstr "Riepilogo"

#: live-installer/frontend/gtk_interface.py:357
msgid "Check that everything is correct"
msgstr ""

#: live-installer/frontend/gtk_interface.py:359
msgid "Installing"
msgstr "Installazione in corso"

#: live-installer/frontend/gtk_interface.py:359
msgid "Please wait..."
msgstr ""

#: live-installer/frontend/gtk_interface.py:362
msgid "Quit"
msgstr "Esci"

#: live-installer/frontend/gtk_interface.py:363
msgid "Back"
msgstr "Indietro"

#: live-installer/frontend/gtk_interface.py:364
#: live-installer/frontend/gtk_interface.py:1158
msgid "Next"
msgstr "Successivo"

#: live-installer/frontend/gtk_interface.py:380
msgid "Type here to test your keyboard layout"
msgstr "Digita qui per testare il layout della tua tastiera"

#: live-installer/frontend/gtk_interface.py:406
#, fuzzy
msgid "Erase a disk and install system on it."
msgstr "Cancella un disco e installa 17G su di esso."

#: live-installer/frontend/gtk_interface.py:419
#, fuzzy
msgid "Manually create, resize or choose partitions for system."
msgstr "Crea, ridimensiona o scegli manualmente le partizioni per 17G."

#: live-installer/frontend/gtk_interface.py:421
msgid "Remove Windows & Install"
msgstr ""

#: live-installer/frontend/gtk_interface.py:423
msgid "Remove existsing windows and install system on it."
msgstr ""

#: live-installer/frontend/gtk_interface.py:432
msgid "Refresh"
msgstr "Aggiorna"

#: live-installer/frontend/gtk_interface.py:434
msgid "Device"
msgstr "Periferica"

#: live-installer/frontend/gtk_interface.py:435
msgid "Type"
msgstr "Tipo"

#: live-installer/frontend/gtk_interface.py:436
msgid "Operating system"
msgstr "Sistema operativo"

#: live-installer/frontend/gtk_interface.py:437
msgid "Mount point"
msgstr "Punto di mount"

#: live-installer/frontend/gtk_interface.py:438
msgid "Format as"
msgstr "Formatta come"

#: live-installer/frontend/gtk_interface.py:439
msgid "Size"
msgstr "Dimensione"

#: live-installer/frontend/gtk_interface.py:597
msgid "Quit?"
msgstr "Uscire?"

#: live-installer/frontend/gtk_interface.py:598
msgid "Are you sure you want to quit the installer?"
msgstr "Sei sicuro di voler uscire dall'installazione?"

#: live-installer/frontend/gtk_interface.py:854
msgid "Please choose a language"
msgstr "Scegli una lingua"

#: live-installer/frontend/gtk_interface.py:890
#, fuzzy
msgid "Please provide a kayboard layout for your computer."
msgstr "Inserisci un nome per il tuo computer."

#: live-installer/frontend/gtk_interface.py:899
msgid "Please provide your full name."
msgstr "Inserisci il tuo nome."

#: live-installer/frontend/gtk_interface.py:904
msgid "Please provide a name for your computer."
msgstr "Inserisci un nome per il tuo computer."

#: live-installer/frontend/gtk_interface.py:908
msgid "Please provide a username."
msgstr "Inserisci un nome utente."

#: live-installer/frontend/gtk_interface.py:913
msgid "Please provide a password for your user account."
msgstr "Inserisci una password per il tuo account."

#: live-installer/frontend/gtk_interface.py:917
#, fuzzy
msgid "Your passwords is too short."
msgstr "Le tue password non coincidono."

#: live-installer/frontend/gtk_interface.py:921
#, fuzzy
msgid "Your passwords is not strong."
msgstr "Le tue password non coincidono."

#: live-installer/frontend/gtk_interface.py:925
msgid "Your passwords do not match."
msgstr "Le tue password non coincidono."

#: live-installer/frontend/gtk_interface.py:931
#, fuzzy
msgid "Your username cannot start with numbers."
msgstr "Il nome utente non può contenere spazi bianchi."

#: live-installer/frontend/gtk_interface.py:937
msgid "Your username must be lower case."
msgstr "Il nome utente deve contenere solo caratteri minuscoli."

#: live-installer/frontend/gtk_interface.py:944
msgid "Your username may not contain whitespace characters."
msgstr "Il nome utente non può contenere spazi bianchi."

#: live-installer/frontend/gtk_interface.py:952
msgid "The computer's name must be lower case."
msgstr "Il nome del computer deve essere scritto in minuscolo."

#: live-installer/frontend/gtk_interface.py:959
msgid "The computer's name may not contain whitespace characters."
msgstr "Il nome del computer non deve contenere spazi."

#: live-installer/frontend/gtk_interface.py:978
msgid ""
"Please indicate a filesystem to format the root (/) partition with before "
"proceeding."
msgstr ""
"Indicare un tipo di filesystem con cui formattare la partizione root (/) "
"prima di continuare."

#: live-installer/frontend/gtk_interface.py:982
msgid "Please select a root (/) partition."
msgstr "Seleziona una partizione root (/)."

#: live-installer/frontend/gtk_interface.py:983
#, fuzzy, python-format
msgid ""
"A root partition is needed to install %s on.\n"
"\n"
" - Mount point: /\n"
" - Recommended size: 30GB\n"
" - Recommended filesystem format: ext4\n"
"\n"
"Note: The timeshift btrfs snapshots feature requires the use of:\n"
" - subvolume Mount-point /@\n"
" - btrfs as filesystem format\n"
msgstr ""
"Per installare Linux Mint è necessaria una partizione di root.\n"
"\n"
"  - Punto di montaggio: /\n"
"  - Dimensioni consigliate: 30 GB\n"
"  - Formato file system consigliato: ext4\n"
"\n"
"Nota: la funzione snapshot btrfs di timeshift richiede l'uso di:\n"
"  - Punto di montaggio del sottovolume /@\n"
"  - btrfs come formato del filesystem\n"

#: live-installer/frontend/gtk_interface.py:1001
msgid "The EFI partition is not bootable. Please edit the partition flags."
msgstr ""
"La partizione EFI non è avviabile, per favore modifica i flag della "
"partizione."

#: live-installer/frontend/gtk_interface.py:1006
msgid "The EFI partition is too small. It must be at least 35MB."
msgstr "La partizione EFI è troppo piccola. Deve essere di almeno 35MB."

#: live-installer/frontend/gtk_interface.py:1012
#: live-installer/frontend/gtk_interface.py:1017
msgid "The EFI partition must be formatted as vfat."
msgstr "La partizione EFI deve essere formattata in vfat."

#: live-installer/frontend/gtk_interface.py:1021
msgid "Please select an EFI partition."
msgstr "Per piacere selezionare una partizione EFI."

#: live-installer/frontend/gtk_interface.py:1022
msgid ""
"An EFI system partition is needed with the following requirements:\n"
"\n"
" - Mount point: /boot/efi\n"
" - Partition flags: Bootable\n"
" - Size: at least 35MB (100MB or more recommended)\n"
" - Format: vfat or fat32\n"
"\n"
"To ensure compatibility with Windows we recommend you use the first "
"partition of the disk as the EFI system partition.\n"
" "
msgstr ""
"È necessaria una partizione di sistema EFI con i seguenti requisiti:\n"
"\n"
"  - Punto di montaggio: /boot/efi\n"
"  - Flag di partizione: Avviabile\n"
"  - Dimensioni: minimo 35 MB (almeno 100 MB consigliati)\n"
"  - Formato: vfat o fat32\n"
"\n"
"Per garantire la compatibilità con Windows, si consiglia di utilizzare la "
"prima partizione del disco come partizione di sistema EFI.\n"
" "

#: live-installer/frontend/gtk_interface.py:1064
msgid "Please select a disk."
msgstr "Seleziona un disco."

#: live-installer/frontend/gtk_interface.py:1070
msgid "Please provide a passphrase for the encryption."
msgstr "Fornisci una frase di accesso per la crittografia."

#: live-installer/frontend/gtk_interface.py:1073
msgid "Your passphrases do not match."
msgstr "Le tue frasi di accesso non corrispondono."

#: live-installer/frontend/gtk_interface.py:1077
msgid "Warning"
msgstr "Attenzione"

#: live-installer/frontend/gtk_interface.py:1078
#, python-format
msgid "This will delete all the data on %s. Are you sure?"
msgstr "Questo eliminerà tutti i dati su %s. Vuoi proseguire?"

#: live-installer/frontend/gtk_interface.py:1139
msgid "Install"
msgstr "Installa"

#: live-installer/frontend/gtk_interface.py:1147
#, fuzzy
msgid "Please provide a device to install grub."
msgstr "Inserisci un nome utente."

#: live-installer/frontend/gtk_interface.py:1186
msgid "Localization"
msgstr "Localizzazione"

#: live-installer/frontend/gtk_interface.py:1187
msgid "Language: "
msgstr "Lingua: "

#: live-installer/frontend/gtk_interface.py:1188
msgid "Timezone: "
msgstr "Fuso orario: "

#: live-installer/frontend/gtk_interface.py:1189
msgid "Keyboard layout: "
msgstr "Disposizione della tastiera: "

#: live-installer/frontend/gtk_interface.py:1193
msgid "User settings"
msgstr "Impostazioni utente"

#: live-installer/frontend/gtk_interface.py:1194
msgid "Real name: "
msgstr "Nome reale: "

#: live-installer/frontend/gtk_interface.py:1195
msgid "Username: "
msgstr "Nome utente: "

#: live-installer/frontend/gtk_interface.py:1197
msgid "Password: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1199
msgid "Automatic login: "
msgstr "Accesso automatico: "

#: live-installer/frontend/gtk_interface.py:1199
#: live-installer/frontend/gtk_interface.py:1202
#: live-installer/frontend/gtk_interface.py:1225
#: live-installer/frontend/gtk_interface.py:1229
msgid "enabled"
msgstr "attivato"

#: live-installer/frontend/gtk_interface.py:1200
#: live-installer/frontend/gtk_interface.py:1203
#: live-installer/frontend/gtk_interface.py:1226
#: live-installer/frontend/gtk_interface.py:1229
msgid "disabled"
msgstr "disattivato"

#: live-installer/frontend/gtk_interface.py:1202
msgid "Home encryption: "
msgstr "Crittografia della Home: "

#: live-installer/frontend/gtk_interface.py:1204
msgid "System settings"
msgstr "Impostazioni di sistema"

#: live-installer/frontend/gtk_interface.py:1205
msgid "Computer's name: "
msgstr "Nome del computer: "

#: live-installer/frontend/gtk_interface.py:1208
#: live-installer/frontend/gtk_interface.py:1210
msgid "Bios type: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1212
#, fuzzy
msgid "Install updates after installation"
msgstr "Installazione Automatica"

#: live-installer/frontend/gtk_interface.py:1213
msgid "Filesystem operations"
msgstr "Operazioni sul filesystem"

#: live-installer/frontend/gtk_interface.py:1214
#, python-format
msgid "Install bootloader on %s"
msgstr "Installare bootloader in %s"

#: live-installer/frontend/gtk_interface.py:1215
msgid "Do not install bootloader"
msgstr "Non installare il bootloader"

#: live-installer/frontend/gtk_interface.py:1217
msgid "Use already-mounted /target."
msgstr "Utilizzare il /target già montato."

#: live-installer/frontend/gtk_interface.py:1222
#, python-format
msgid "Automated installation on %s"
msgstr "Installazione automatizzata su %s"

#: live-installer/frontend/gtk_interface.py:1225
msgid "LVM: "
msgstr "LVM: "

#: live-installer/frontend/gtk_interface.py:1228
msgid "Disk Encryption: "
msgstr "Crittografia Disco: "

#: live-installer/frontend/gtk_interface.py:1232
#, python-format
msgid "Format %(path)s as %(filesystem)s"
msgstr "Formattare %(path)s come %(filesystem)s"

#: live-installer/frontend/gtk_interface.py:1236
#, python-format
msgid "Mount %(path)s as %(mount)s"
msgstr "Montare %(path)s come %(mount)s"

#: live-installer/frontend/gtk_interface.py:1248
#: live-installer/installer.py:821
msgid "Installation finished"
msgstr "Installazione completata"

#: live-installer/frontend/gtk_interface.py:1249
msgid ""
"The installation is now complete. Do you want to restart your computer to "
"use the new system?"
msgstr ""
"L' installazione è completata. Vuoi riavviare il computer per usare il nuovo "
"sistema?"

#: live-installer/frontend/gtk_interface.py:1280
#: live-installer/frontend/gtk_interface.py:1284
#: live-installer/frontend/gtk_interface.py:1293
#: live-installer/frontend/gtk_interface.py:1302
msgid "Installation error"
msgstr "Errore di installazione"

#: live-installer/main.py:34
msgid "You must be root!"
msgstr ""

#: live-installer/installer.py:133 live-installer/installer.py:154
#, python-format
msgid "Copying /%s"
msgstr "Copia di /%s"

#: live-installer/installer.py:134
#, python-format
msgid "rsync exited with return code: %s"
msgstr ""

#: live-installer/installer.py:138
msgid "Extracting rootfs."
msgstr ""

#: live-installer/installer.py:174
msgid "Entering the system ..."
msgstr "Avvio del Sistema..."

#: live-installer/installer.py:204
msgid "Adding new user to the system"
msgstr "Aggiunta nuovo utente al sistema"

#: live-installer/installer.py:255
msgid "Writing filesystem mount information to /etc/fstab"
msgstr "Scrittura informazioni di montaggio del filesystem in /etc/fstab"

#: live-installer/installer.py:261 live-installer/installer.py:417
#: live-installer/installer.py:432
#, python-format
msgid "Mounting %(partition)s on %(mountpoint)s"
msgstr "Montaggio di %(partition)s in %(mountpoint)s"

#: live-installer/installer.py:298
#, python-format
msgid "Filling %s with random data (please be patient, this can take hours...)"
msgstr ""
"Sto riempiendo %s con dati casuali (sii paziente, può richiedere alcune "
"ore...)"

#: live-installer/installer.py:304
#, python-format
msgid "Creating partitions on %s"
msgstr "Creazione delle partizioni su %s"

#: live-installer/installer.py:369
#, python-format
msgid "Formatting %(partition)s as %(format)s ..."
msgstr "Formattazione di %(partition)s come %(format)s"

#: live-installer/installer.py:558
msgid "Setting hostname"
msgstr "Impostazione dell'hostname"

#: live-installer/installer.py:583
msgid "Setting locale"
msgstr "Impostazione della localizzazione"

#: live-installer/installer.py:604
#, fuzzy
msgid "Setting timezone"
msgstr "Impostazione dell'hostname"

#: live-installer/installer.py:637
msgid "Setting keyboard options"
msgstr "Impostazione delle opzioni della tastiera"

#: live-installer/installer.py:733
msgid "Trying to install updates"
msgstr ""

#: live-installer/installer.py:737
msgid "Clearing package manager"
msgstr ""

#: live-installer/installer.py:754
msgid "Generating initramfs"
msgstr ""

#: live-installer/installer.py:759
msgid "Preparing bootloader installation"
msgstr ""

#: live-installer/installer.py:771
msgid "Installing bootloader"
msgstr "Installazione del bootloader"

#: live-installer/installer.py:783
msgid "Configuring bootloader"
msgstr "Configurazione del bootloader"

#: live-installer/installer.py:791
msgid ""
"WARNING: The grub bootloader was not configured properly! You need to "
"configure it manually."
msgstr ""
"ATTENZIONE: Il bootloader grub non è stato configurato correttamente! È "
"necessaria una configurazione manuale."

#: live-installer/installer.py:844
msgid "Checking bootloader"
msgstr "Controllo del bootloader"

#: live-installer/installer.py:876
msgid "Failed to run command (Exited with {}):"
msgstr ""

#, fuzzy
#~ msgid "Select additional options"
#~ msgstr "Impostazione delle opzioni della tastiera"

#~ msgid ""
#~ "* Your username, your computer's name and your password should only "
#~ "contain Latin characters. In addition to your selected layout, English "
#~ "(US) is set as the default. You can switch layouts by pressing both Ctrl "
#~ "keys together."
#~ msgstr ""
#~ "* Il nome utente, il nome del computer e la password devono contenere "
#~ "solo caratteri latini. Oltre al layout selezionato, l'inglese (USA) è "
#~ "impostato come predefinito. Puoi cambiare layout premendo entrambi i "
#~ "tasti Ctrl insieme."

#~ msgid ""
#~ "ERROR: You must first manually mount your target filesystem(s) at /target "
#~ "to do a custom install!"
#~ msgstr ""
#~ "ERRORE: Devi prima montare manualmente il filesystem di destinazione in /"
#~ "destinazione per eseguire una installazione personalizzata!"

#~ msgid "Removing live configuration (packages)"
#~ msgstr "Rimozione della configurazione live (pacchetti)"

#~ msgid "ERROR: the use of @subvolumes is limited to btrfs"
#~ msgstr "ERRORE: l'utilizzo di @subvolumes è limitato a btrfs"

#~ msgid "Localizing packages"
#~ msgstr "Localizzazione pacchetti"

#~ msgid "Cleaning APT"
#~ msgstr "Pulizia di APT"

#~ msgid "Advanced options"
#~ msgstr "Opzioni avanzate"

#~ msgid "Expert mode"
#~ msgstr "Modalità esperto"

#~ msgid "Installation paused"
#~ msgstr "Installazione in pausa"

#~ msgid ""
#~ "You selected to manage your partitions manually, this feature is for "
#~ "ADVANCED USERS ONLY."
#~ msgstr ""
#~ "Hai scelto di gestire manualmente le tue partizioni, questa funzione è "
#~ "SOLO PER UTENTI ESPERTI."

#~ msgid "Before continuing, mount your target filesystem(s) on /target."
#~ msgstr ""
#~ "Prima di continuare, montare il/i filesystem di destinazione su /"
#~ "destinazione."

#~ msgid ""
#~ "Do NOT mount virtual devices such as /dev, /proc, /sys, etc on /target/."
#~ msgstr ""
#~ "NON montare periferiche virtuali come /dev, /proc, /sys, ecc. in /target/."

#~ msgid ""
#~ "During the install, you will be given time to chroot into /target and "
#~ "install any packages that will be needed to boot your new system."
#~ msgstr ""
#~ "Durante l'installazione avrai tempo per fare chroot in /target ed "
#~ "installare qualsiasi pacchetto che sia necessario per avviare il tuo "
#~ "nuovo sistema."

#~ msgid ""
#~ "During the install, you will be required to write your own /etc/fstab."
#~ msgstr ""
#~ "Durante l'installazione ti sarà chiesto di scrivere il tuo /etc/fstab."

#~ msgid "Do the following and then click Next to finish installation:"
#~ msgstr ""
#~ "Effettua quanto segue e quindi fai clic su Avanti per terminare "
#~ "l'installazione:"

#~ msgid ""
#~ "Create /target/etc/fstab for the filesystems as they will be mounted in "
#~ "your new system, matching those currently mounted at /target (without "
#~ "using the /target prefix in the mount paths themselves)."
#~ msgstr ""
#~ "Creare /target/etc/fstab per i filesystem corrispondenti a quelli "
#~ "attualmente montati in /target (senza usare il prefisso /target nei "
#~ "percorsi di mount stessi) in modo che poi siano montati nel nuovo sistema."

#~ msgid ""
#~ "Install any packages that may be needed for first boot (mdadm, "
#~ "cryptsetup, dmraid, etc) by calling \"sudo chroot /target\" followed by "
#~ "the relevant apt-get/aptitude installations."
#~ msgstr ""
#~ "Installa i pacchetti che potrebbero essere necessari per il primo avvio "
#~ "(mdadm, cryptsetup, dmraid, ecc.) eseguendo \"sudo chroot /target\" "
#~ "seguito dalle corrispondenti installazioni con apt-get/aptitude"

#~ msgid ""
#~ "Note that in order for update-initramfs to work properly in some cases "
#~ "(such as dm-crypt), you may need to have drives currently mounted using "
#~ "the same block device name as they appear in /target/etc/fstab."
#~ msgstr ""
#~ "Nota che per fare in modo che update-initramfs funzioni correttamente in "
#~ "alcuni casi (come dm-crypt), potrebbe essere necessario che i dischi "
#~ "siano montati usando lo stesso nome del dispositivo a blocchi con il "
#~ "quale compaiono in /target/etc/fstab."

#~ msgid ""
#~ "Double-check that your /target/etc/fstab is correct, matches what your "
#~ "new system will have at first boot, and matches what is currently mounted "
#~ "at /target."
#~ msgstr ""
#~ "Controlla attentamente che il tuo /target/etc/fstab sia corretto, che "
#~ "corrisponda a quello che il tuo sistema avrà al primo avvio, e che "
#~ "corrisponda a ciò che è montato in /target."

#~ msgid "A root subvolume (/@) requires to format the partition with btrfs."
#~ msgstr ""
#~ "Un sottovolume di root (/@) richiede di formattare la partizione con "
#~ "btrfs."

#~ msgid ""
#~ "A home subvolume (/@home) requires the use of a btrfs formatted partition."
#~ msgstr ""
#~ "Un sottovolume home (/@home) richiede l'uso di una partizione formattata "
#~ "btrfs."

#~ msgid ""
#~ "The installation is now paused. Please read the instructions on the page "
#~ "carefully before clicking Next to finish the installation."
#~ msgstr ""
#~ "L'installazione è ora in pausa. Leggere attentamente le istruzioni sulla "
#~ "pagina prima di fare clic su Avanti per completare l'installazione."

#~ msgid "Assign to /home"
#~ msgstr "Assegnare a /home"

#~ msgid "Assign to /boot/efi"
#~ msgstr "Assegnare a /boot/efi"
