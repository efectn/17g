# Urdu translation for linuxmint
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-23 20:34+0300\n"
"PO-Revision-Date: 2016-12-06 13:44+0000\n"
"Last-Translator: Waqar Ahmed <waqar.17a@gmail.com>\n"
"Language-Team: Urdu <ur@li.org>\n"
"Language: ur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-03-13 11:20+0000\n"
"X-Generator: Launchpad (build 3a6db24bbe7280ec09bae73384238390fcc98ad3)\n"

#: live-installer/resources/interface2.ui:66
#: live-installer/resources/interface.ui:131
msgid "Welcome to the 17g Installer."
msgstr ""

#: live-installer/resources/interface2.ui:83
#: live-installer/resources/interface.ui:148
#: live-installer/frontend/tui_interface.py:25
#: live-installer/frontend/gtk_interface.py:370
msgid ""
"This program will ask you some questions and set up system on your computer."
msgstr ""

#: live-installer/resources/interface2.ui:179
#: live-installer/resources/interface2.ui:1437
#: live-installer/resources/interface2.ui:1883
#: live-installer/resources/interface2.ui:1895
#: live-installer/resources/interface2.ui:1907
#: live-installer/resources/interface.ui:244
#: live-installer/resources/interface.ui:1510
#: live-installer/resources/interface.ui:1858
#: live-installer/resources/interface.ui:1870
#: live-installer/resources/interface.ui:1882
msgid "label"
msgstr ""

#: live-installer/resources/interface2.ui:214
#: live-installer/resources/interface.ui:279
msgid "Continent"
msgstr ""

#: live-installer/resources/interface2.ui:228
#: live-installer/resources/interface.ui:293
#: live-installer/frontend/tui_interface.py:41
#: live-installer/frontend/gtk_interface.py:347
msgid "Timezone"
msgstr "علاقہ وقت"

#: live-installer/resources/interface2.ui:271
#: live-installer/resources/interface.ui:338
#: live-installer/frontend/gtk_interface.py:378
msgid "Keyboard Model:"
msgstr ""

#: live-installer/resources/interface2.ui:364
#: live-installer/resources/interface.ui:435
msgid "Type here to test your keyboard"
msgstr ""

#: live-installer/resources/interface2.ui:411
#: live-installer/resources/interface.ui:482
#: live-installer/frontend/gtk_interface.py:404
msgid "Automated Installation"
msgstr ""

#: live-installer/resources/interface2.ui:427
#: live-installer/resources/interface.ui:498
msgid "Erase a disk and install LMDE on it."
msgstr ""

#: live-installer/resources/interface2.ui:455
#: live-installer/resources/interface.ui:526
#: live-installer/frontend/gtk_interface.py:407
msgid "Disk:"
msgstr ""

#: live-installer/resources/interface2.ui:505
#: live-installer/resources/interface.ui:576
#: live-installer/frontend/gtk_interface.py:415
msgid "Use LVM (Logical Volume Management)"
msgstr ""

#: live-installer/resources/interface2.ui:552
#: live-installer/resources/interface.ui:623
#: live-installer/frontend/gtk_interface.py:409
msgid "Encrypt the operating system"
msgstr ""

#: live-installer/resources/interface2.ui:582
#: live-installer/resources/interface.ui:653
#: live-installer/frontend/gtk_interface.py:411
msgid "Passphrase"
msgstr ""

#: live-installer/resources/interface2.ui:598
#: live-installer/resources/interface.ui:669
#: live-installer/frontend/gtk_interface.py:413
msgid "Confirm passphrase"
msgstr ""

#: live-installer/resources/interface2.ui:632
#: live-installer/resources/interface.ui:703
#: live-installer/frontend/gtk_interface.py:428
msgid "This provides extra security but it can take hours."
msgstr ""

#: live-installer/resources/interface2.ui:646
#: live-installer/resources/interface.ui:717
#: live-installer/frontend/gtk_interface.py:426
msgid "Fill the disk with random data"
msgstr ""

#: live-installer/resources/interface2.ui:731
#: live-installer/resources/interface.ui:802
#: live-installer/frontend/gtk_interface.py:417
msgid "Manual Partitioning"
msgstr ""

#: live-installer/resources/interface2.ui:747
#: live-installer/resources/interface.ui:818
msgid "Manually create, resize or choose partitions for LMDE."
msgstr ""

#: live-installer/resources/interface2.ui:812
#: live-installer/resources/interface.ui:883
msgid "Remove windows & Install"
msgstr ""

#: live-installer/resources/interface2.ui:828
#: live-installer/resources/interface.ui:899
msgid "Remove existsing windows and install LMDE on it."
msgstr ""

#: live-installer/resources/interface2.ui:891
#: live-installer/resources/interface.ui:962
#: live-installer/frontend/gtk_interface.py:451
msgid "Install system with updates"
msgstr ""

#: live-installer/resources/interface2.ui:907
#: live-installer/resources/interface.ui:978
#: live-installer/frontend/gtk_interface.py:452
msgid "If you connect internet, updates will install."
msgstr ""

#: live-installer/resources/interface2.ui:1008
#: live-installer/resources/interface.ui:1079
#: live-installer/frontend/gtk_interface.py:445
msgid "Install the GRUB boot menu on:"
msgstr ""

#: live-installer/resources/interface2.ui:1066
#: live-installer/resources/interface.ui:1137
#: live-installer/frontend/gtk_interface.py:431
msgid "Edit partitions"
msgstr "پارٹیشن میں ترمیم"

#: live-installer/resources/interface2.ui:1122
#: live-installer/resources/interface.ui:1193
#: live-installer/frontend/gtk_interface.py:383
msgid "Your name:"
msgstr ""

#: live-installer/resources/interface2.ui:1165
#: live-installer/resources/interface.ui:1236
#: live-installer/frontend/gtk_interface.py:385
msgid "Your computer's name:"
msgstr ""

#: live-installer/resources/interface2.ui:1180
#: live-installer/resources/interface.ui:1251
#: live-installer/frontend/gtk_interface.py:389
msgid "Pick a username:"
msgstr ""

#: live-installer/resources/interface2.ui:1193
#: live-installer/resources/interface.ui:1264
#: live-installer/frontend/gtk_interface.py:387
msgid "The name it uses when it talks to other computers."
msgstr ""

#: live-installer/resources/interface2.ui:1226
#: live-installer/resources/interface.ui:1297
#: live-installer/frontend/gtk_interface.py:391
msgid "Choose a password:"
msgstr ""

#: live-installer/resources/interface2.ui:1271
#: live-installer/resources/interface.ui:1342
#: live-installer/frontend/gtk_interface.py:393
msgid "Confirm your password:"
msgstr ""

#: live-installer/resources/interface2.ui:1289
#: live-installer/resources/interface.ui:1360
#: live-installer/frontend/gtk_interface.py:396
msgid "Log in automatically"
msgstr "خودکار لاگ ان کر دیں"

#: live-installer/resources/interface2.ui:1304
#: live-installer/resources/interface.ui:1375
#: live-installer/frontend/gtk_interface.py:398
msgid "Require my password to log in"
msgstr ""

#: live-installer/resources/interface2.ui:1319
#: live-installer/resources/interface.ui:1390
#: live-installer/frontend/gtk_interface.py:400
msgid "Encrypt my home folder"
msgstr ""

#: live-installer/resources/interface2.ui:1452
#: live-installer/resources/interface.ui:1525
msgid "0.0"
msgstr ""

#: live-installer/resources/interface2.ui:1484
#: live-installer/resources/interface.ui:1557
#: live-installer/frontend/gtk_interface.py:454
msgid "Please do not turn off your computer during the installation process."
msgstr ""

#: live-installer/resources/interface2.ui:1552
#: live-installer/resources/interface.ui:1607
#: live-installer/resources/welcome.ui:206
msgid "17g team"
msgstr ""

#: live-installer/resources/interface2.ui:1755
#: live-installer/resources/interface2.ui:1795
#: live-installer/resources/interface.ui:54
#: live-installer/resources/interface.ui:73
#: live-installer/frontend/gtk_interface.py:343
msgid "Welcome"
msgstr ""

#: live-installer/resources/interface2.ui:1919
#: live-installer/resources/interface.ui:1894
msgid "/dev/sda1"
msgstr ""

#: live-installer/resources/welcome.ui:61
msgid "Try 17g"
msgstr ""

#: live-installer/resources/welcome.ui:85
msgid " "
msgstr ""

#: live-installer/resources/welcome.ui:121
#: live-installer/frontend/welcome.py:34
#, fuzzy
msgid "Install to Hard Drive"
msgstr "ڈرائیور انسٹال کر رہا ہے"

#: live-installer/resources/welcome.ui:166
msgid "You are currently running 17g from live media."
msgstr ""

#: live-installer/resources/welcome.ui:178
msgid ""
"You can install 17g now, or chose \"Install to Hard Drive\" in the "
"Appication Menu later."
msgstr ""

#: live-installer/resources/welcome.ui:228
msgid "Welcome to 17g"
msgstr ""

#: live-installer/frontend/partitioning.py:59
msgid "B"
msgstr "ب"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:412
msgid "kB"
msgstr "ک.ب"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:412
msgid "MB"
msgstr "م.ب"

#: live-installer/frontend/partitioning.py:59
#: live-installer/frontend/partitioning.py:413
msgid "GB"
msgstr "گ.ب"

#: live-installer/frontend/partitioning.py:60
#: live-installer/frontend/partitioning.py:413
msgid "TB"
msgstr "ٹ.ب"

#: live-installer/frontend/partitioning.py:67
msgid "Removable:"
msgstr "قابلِ علیحدگی:"

#: live-installer/frontend/partitioning.py:174
msgid "Edit"
msgstr "ترمیم"

#: live-installer/frontend/partitioning.py:179
#: live-installer/frontend/partitioning.py:183
#: live-installer/frontend/partitioning.py:187
#: live-installer/frontend/partitioning.py:194
#: live-installer/frontend/partitioning.py:203
#, python-format
msgid "Assign to %s"
msgstr ""

#: live-installer/frontend/partitioning.py:267
msgid "Installation Tool"
msgstr "انسٹالیشن کا اوزار"

#: live-installer/frontend/partitioning.py:268
#, python-format
msgid ""
"No partition table was found on the hard drive: %s. Do you want the "
"installer to create a set of partitions for you? Note: This will ERASE ALL "
"DATA present on this disk."
msgstr ""

#: live-installer/frontend/partitioning.py:320
#: live-installer/frontend/partitioning.py:466
#: live-installer/frontend/gtk_interface.py:440
msgid "Free space"
msgstr "خالی جگہ"

#: live-installer/frontend/partitioning.py:336
#: live-installer/frontend/gtk_interface.py:331
#: live-installer/frontend/gtk_interface.py:334
#: live-installer/frontend/gtk_interface.py:853
#: live-installer/frontend/gtk_interface.py:889
#: live-installer/frontend/gtk_interface.py:964
#: live-installer/frontend/gtk_interface.py:977
#: live-installer/frontend/gtk_interface.py:982
#: live-installer/frontend/gtk_interface.py:1000
#: live-installer/frontend/gtk_interface.py:1005
#: live-installer/frontend/gtk_interface.py:1011
#: live-installer/frontend/gtk_interface.py:1016
#: live-installer/frontend/gtk_interface.py:1021
#: live-installer/frontend/gtk_interface.py:1075
#: live-installer/frontend/gtk_interface.py:1146
msgid "Installer"
msgstr ""

#: live-installer/frontend/partitioning.py:352
#, python-format
msgid ""
"The partition table couldn't be written for %s. Restart the computer and try "
"again."
msgstr ""

#: live-installer/frontend/partitioning.py:399
#, python-format
msgid ""
"The partition %s could not be created. The installation will stop. Restart "
"the computer and try again."
msgstr ""

#: live-installer/frontend/partitioning.py:464
msgid "Logical partition"
msgstr ""

#: live-installer/frontend/partitioning.py:465
msgid "Extended partition"
msgstr ""

#: live-installer/frontend/partitioning.py:469
msgid "Unknown"
msgstr "نا معلوم"

#: live-installer/frontend/partitioning.py:526
msgid "bootloader/recovery"
msgstr ""

#: live-installer/frontend/partitioning.py:538
#, fuzzy
msgid "EFI System Partition"
msgstr "پارٹیشن کی ترمیم"

#: live-installer/frontend/partitioning.py:561
msgid "Edit partition"
msgstr "پارٹیشن کی ترمیم"

#: live-installer/frontend/partitioning.py:563
msgid "Device:"
msgstr "آلہ:"

#: live-installer/frontend/partitioning.py:565
msgid "Format as:"
msgstr "فارمیٹ کریں بطور:"

#: live-installer/frontend/partitioning.py:567
msgid "Mount point:"
msgstr "ماؤنٹ پوائنٹ"

#: live-installer/frontend/partitioning.py:568
msgid "Cancel"
msgstr ""

#: live-installer/frontend/partitioning.py:569
#: live-installer/frontend/dialogs.py:35 live-installer/frontend/dialogs.py:48
#: live-installer/frontend/dialogs.py:54
msgid "OK"
msgstr ""

#: live-installer/frontend/tui_interface.py:23
#: live-installer/frontend/gtk_interface.py:368
#, python-format
msgid "Welcome to the %s Installer."
msgstr ""

#: live-installer/frontend/tui_interface.py:28
#: live-installer/frontend/gtk_interface.py:345
msgid "What language would you like to use?"
msgstr ""

#: live-installer/frontend/tui_interface.py:33
#: live-installer/frontend/gtk_interface.py:106
#: live-installer/frontend/gtk_interface.py:345
#: live-installer/frontend/gtk_interface.py:373
msgid "Language"
msgstr "زبان"

#: live-installer/frontend/tui_interface.py:37
#: live-installer/frontend/gtk_interface.py:347
msgid "Where are you?"
msgstr ""

#: live-installer/frontend/tui_interface.py:45
msgid "What keyboard would you like to use?"
msgstr ""

#: live-installer/frontend/tui_interface.py:52
#, fuzzy
msgid "Keyboard Model"
msgstr "کیبورڈ خاکہ"

#: live-installer/frontend/timezones.py:142
#, fuzzy
msgid "Select timezone"
msgstr "علاقہ وقت"

#: live-installer/frontend/dialogs.py:41
msgid "No"
msgstr ""

#: live-installer/frontend/dialogs.py:42
msgid "Yes"
msgstr ""

#: live-installer/frontend/welcome.py:32
#, python-format
msgid "Try %s"
msgstr ""

#: live-installer/frontend/welcome.py:36
#, python-format
msgid "You are currently running %s from live media."
msgstr ""

#: live-installer/frontend/welcome.py:38
#, python-format
msgid ""
"You can install %s now, or chose \"Install to Hard Drive\" in the Appication "
"Menu later."
msgstr ""

#: live-installer/frontend/welcome.py:39
#, python-format
msgid "Welcome to %s"
msgstr ""

#: live-installer/frontend/gtk_interface.py:97
#: live-installer/frontend/gtk_interface.py:374
msgid "Country"
msgstr "ملک"

#: live-installer/frontend/gtk_interface.py:217
msgid "Layout"
msgstr "وضع کاری"

#: live-installer/frontend/gtk_interface.py:225
msgid "Variant"
msgstr "مختلف"

#: live-installer/frontend/gtk_interface.py:243
msgid "Calculating file indexes ..."
msgstr ""

#: live-installer/frontend/gtk_interface.py:349
msgid "Keyboard layout"
msgstr "کیبورڈ خاکہ"

#: live-installer/frontend/gtk_interface.py:349
msgid "What is your keyboard layout?"
msgstr ""

#: live-installer/frontend/gtk_interface.py:351
msgid "User account"
msgstr ""

#: live-installer/frontend/gtk_interface.py:351
msgid "Who are you?"
msgstr ""

#: live-installer/frontend/gtk_interface.py:353
msgid "Installation Type"
msgstr ""

#: live-installer/frontend/gtk_interface.py:353
#: live-installer/frontend/gtk_interface.py:355
#, fuzzy
msgid "Where do you want to install system?"
msgstr "کیا آپ واقعی بند کرنا چاہتے ہیں؟"

#: live-installer/frontend/gtk_interface.py:355
msgid "Partitioning"
msgstr "پارٹیشن کر رہا ہے"

#: live-installer/frontend/gtk_interface.py:357
msgid "Summary"
msgstr "خلاصہ"

#: live-installer/frontend/gtk_interface.py:357
msgid "Check that everything is correct"
msgstr ""

#: live-installer/frontend/gtk_interface.py:359
msgid "Installing"
msgstr ""

#: live-installer/frontend/gtk_interface.py:359
msgid "Please wait..."
msgstr ""

#: live-installer/frontend/gtk_interface.py:362
msgid "Quit"
msgstr ""

#: live-installer/frontend/gtk_interface.py:363
msgid "Back"
msgstr ""

#: live-installer/frontend/gtk_interface.py:364
#: live-installer/frontend/gtk_interface.py:1158
msgid "Next"
msgstr ""

#: live-installer/frontend/gtk_interface.py:380
msgid "Type here to test your keyboard layout"
msgstr ""

#: live-installer/frontend/gtk_interface.py:406
msgid "Erase a disk and install system on it."
msgstr ""

#: live-installer/frontend/gtk_interface.py:419
msgid "Manually create, resize or choose partitions for system."
msgstr ""

#: live-installer/frontend/gtk_interface.py:421
msgid "Remove Windows & Install"
msgstr ""

#: live-installer/frontend/gtk_interface.py:423
msgid "Remove existsing windows and install system on it."
msgstr ""

#: live-installer/frontend/gtk_interface.py:432
msgid "Refresh"
msgstr "تازہ کریں"

#: live-installer/frontend/gtk_interface.py:434
msgid "Device"
msgstr "ڈیوائس"

#: live-installer/frontend/gtk_interface.py:435
msgid "Type"
msgstr "قسم"

#: live-installer/frontend/gtk_interface.py:436
msgid "Operating system"
msgstr "آپریٹینگ سسٹم"

#: live-installer/frontend/gtk_interface.py:437
msgid "Mount point"
msgstr "ماؤنٹ پوائنٹ"

#: live-installer/frontend/gtk_interface.py:438
msgid "Format as"
msgstr "فارمیٹ کریں بطور"

#: live-installer/frontend/gtk_interface.py:439
msgid "Size"
msgstr "سائز"

#: live-installer/frontend/gtk_interface.py:597
msgid "Quit?"
msgstr "بند کر دیں؟"

#: live-installer/frontend/gtk_interface.py:598
msgid "Are you sure you want to quit the installer?"
msgstr "کیا آپ واقعی بند کرنا چاہتے ہیں؟"

#: live-installer/frontend/gtk_interface.py:854
msgid "Please choose a language"
msgstr "برائے مہربانی زبان منتخب کریں"

#: live-installer/frontend/gtk_interface.py:890
#, fuzzy
msgid "Please provide a kayboard layout for your computer."
msgstr "اپنے کھاتے کے لیے پاس ورڈ فراہم کریں۔"

#: live-installer/frontend/gtk_interface.py:899
msgid "Please provide your full name."
msgstr "اپنا پورا نام فراہم کریں۔"

#: live-installer/frontend/gtk_interface.py:904
msgid "Please provide a name for your computer."
msgstr ""

#: live-installer/frontend/gtk_interface.py:908
msgid "Please provide a username."
msgstr "اپنا اسم صارف(username) فراہم کریں۔"

#: live-installer/frontend/gtk_interface.py:913
msgid "Please provide a password for your user account."
msgstr "اپنے کھاتے کے لیے پاس ورڈ فراہم کریں۔"

#: live-installer/frontend/gtk_interface.py:917
#, fuzzy
msgid "Your passwords is too short."
msgstr "پاس ورڈ آپس میں مل نہیں رہے"

#: live-installer/frontend/gtk_interface.py:921
#, fuzzy
msgid "Your passwords is not strong."
msgstr "پاس ورڈ آپس میں مل نہیں رہے"

#: live-installer/frontend/gtk_interface.py:925
msgid "Your passwords do not match."
msgstr "پاس ورڈ آپس میں مل نہیں رہے"

#: live-installer/frontend/gtk_interface.py:931
#, fuzzy
msgid "Your username cannot start with numbers."
msgstr "آپکے اسم صارف میں whitespace نہیں ہو سکتی۔"

#: live-installer/frontend/gtk_interface.py:937
msgid "Your username must be lower case."
msgstr "آپکا اسم صارف lowercase میں ہونا چاہیے۔"

#: live-installer/frontend/gtk_interface.py:944
msgid "Your username may not contain whitespace characters."
msgstr "آپکے اسم صارف میں whitespace نہیں ہو سکتی۔"

#: live-installer/frontend/gtk_interface.py:952
msgid "The computer's name must be lower case."
msgstr ""

#: live-installer/frontend/gtk_interface.py:959
msgid "The computer's name may not contain whitespace characters."
msgstr ""

#: live-installer/frontend/gtk_interface.py:978
msgid ""
"Please indicate a filesystem to format the root (/) partition with before "
"proceeding."
msgstr ""

#: live-installer/frontend/gtk_interface.py:982
msgid "Please select a root (/) partition."
msgstr ""

#: live-installer/frontend/gtk_interface.py:983
#, python-format
msgid ""
"A root partition is needed to install %s on.\n"
"\n"
" - Mount point: /\n"
" - Recommended size: 30GB\n"
" - Recommended filesystem format: ext4\n"
"\n"
"Note: The timeshift btrfs snapshots feature requires the use of:\n"
" - subvolume Mount-point /@\n"
" - btrfs as filesystem format\n"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1001
msgid "The EFI partition is not bootable. Please edit the partition flags."
msgstr ""
"EFI پارٹیشن بوٹ ہو نے کے قابل نہیں ہے۔ برائے مہربانی پارٹیشن فلیگ کی ترمیم "
"کریں۔"

#: live-installer/frontend/gtk_interface.py:1006
msgid "The EFI partition is too small. It must be at least 35MB."
msgstr ""

#: live-installer/frontend/gtk_interface.py:1012
#: live-installer/frontend/gtk_interface.py:1017
msgid "The EFI partition must be formatted as vfat."
msgstr "EFI پارٹیشن بطور vfat فارمیٹ ہونی چاہیے۔"

#: live-installer/frontend/gtk_interface.py:1021
msgid "Please select an EFI partition."
msgstr "برائے مہربانی ایک EFI پارٹیشن منتخب کریں۔"

#: live-installer/frontend/gtk_interface.py:1022
msgid ""
"An EFI system partition is needed with the following requirements:\n"
"\n"
" - Mount point: /boot/efi\n"
" - Partition flags: Bootable\n"
" - Size: at least 35MB (100MB or more recommended)\n"
" - Format: vfat or fat32\n"
"\n"
"To ensure compatibility with Windows we recommend you use the first "
"partition of the disk as the EFI system partition.\n"
" "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1064
msgid "Please select a disk."
msgstr ""

#: live-installer/frontend/gtk_interface.py:1070
msgid "Please provide a passphrase for the encryption."
msgstr ""

#: live-installer/frontend/gtk_interface.py:1073
msgid "Your passphrases do not match."
msgstr ""

#: live-installer/frontend/gtk_interface.py:1077
msgid "Warning"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1078
#, python-format
msgid "This will delete all the data on %s. Are you sure?"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1139
msgid "Install"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1147
#, fuzzy
msgid "Please provide a device to install grub."
msgstr "اپنا اسم صارف(username) فراہم کریں۔"

#: live-installer/frontend/gtk_interface.py:1186
msgid "Localization"
msgstr "مقامیانہ"

#: live-installer/frontend/gtk_interface.py:1187
msgid "Language: "
msgstr "زبان: "

#: live-installer/frontend/gtk_interface.py:1188
msgid "Timezone: "
msgstr "علاقہ وقت: "

#: live-installer/frontend/gtk_interface.py:1189
msgid "Keyboard layout: "
msgstr "کیبورڈ خاکہ: "

#: live-installer/frontend/gtk_interface.py:1193
msgid "User settings"
msgstr "صارف کی ترتیبات"

#: live-installer/frontend/gtk_interface.py:1194
msgid "Real name: "
msgstr "اصل نام: "

#: live-installer/frontend/gtk_interface.py:1195
msgid "Username: "
msgstr "اسمِ صارف: "

#: live-installer/frontend/gtk_interface.py:1197
msgid "Password: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1199
msgid "Automatic login: "
msgstr "خودکار لاگ ان: "

#: live-installer/frontend/gtk_interface.py:1199
#: live-installer/frontend/gtk_interface.py:1202
#: live-installer/frontend/gtk_interface.py:1225
#: live-installer/frontend/gtk_interface.py:1229
msgid "enabled"
msgstr "فعال کردہ"

#: live-installer/frontend/gtk_interface.py:1200
#: live-installer/frontend/gtk_interface.py:1203
#: live-installer/frontend/gtk_interface.py:1226
#: live-installer/frontend/gtk_interface.py:1229
msgid "disabled"
msgstr "غیر فعال کردہ"

#: live-installer/frontend/gtk_interface.py:1202
msgid "Home encryption: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1204
msgid "System settings"
msgstr "سسٹم کی ترتیبات"

#: live-installer/frontend/gtk_interface.py:1205
msgid "Computer's name: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1208
#: live-installer/frontend/gtk_interface.py:1210
msgid "Bios type: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1212
msgid "Install updates after installation"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1213
msgid "Filesystem operations"
msgstr "فائل سسٹم آپریشن"

#: live-installer/frontend/gtk_interface.py:1214
#, python-format
msgid "Install bootloader on %s"
msgstr "بوٹ لوڈر کو %s پہ انسٹال کر دیں"

#: live-installer/frontend/gtk_interface.py:1215
msgid "Do not install bootloader"
msgstr "بوٹ لوڈر مت انسٹال کریں"

#: live-installer/frontend/gtk_interface.py:1217
msgid "Use already-mounted /target."
msgstr ""

#: live-installer/frontend/gtk_interface.py:1222
#, python-format
msgid "Automated installation on %s"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1225
msgid "LVM: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1228
msgid "Disk Encryption: "
msgstr ""

#: live-installer/frontend/gtk_interface.py:1232
#, python-format
msgid "Format %(path)s as %(filesystem)s"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1236
#, python-format
msgid "Mount %(path)s as %(mount)s"
msgstr ""

#: live-installer/frontend/gtk_interface.py:1248
#: live-installer/installer.py:821
msgid "Installation finished"
msgstr "انسٹالیشن مکمل ہوئی"

#: live-installer/frontend/gtk_interface.py:1249
msgid ""
"The installation is now complete. Do you want to restart your computer to "
"use the new system?"
msgstr "انسٹالیشن مکمل ہو گئی۔ کیا آپ اب کمپیوٹر کو ری۔سٹارٹ کرنا چاہیں گے؟"

#: live-installer/frontend/gtk_interface.py:1280
#: live-installer/frontend/gtk_interface.py:1284
#: live-installer/frontend/gtk_interface.py:1293
#: live-installer/frontend/gtk_interface.py:1302
msgid "Installation error"
msgstr "انسٹالیشن میں مسئلہ"

#: live-installer/main.py:34
msgid "You must be root!"
msgstr ""

#: live-installer/installer.py:133 live-installer/installer.py:154
#, python-format
msgid "Copying /%s"
msgstr ""

#: live-installer/installer.py:134
#, python-format
msgid "rsync exited with return code: %s"
msgstr ""

#: live-installer/installer.py:138
msgid "Extracting rootfs."
msgstr ""

#: live-installer/installer.py:174
msgid "Entering the system ..."
msgstr "سسٹم میں داخل ہو رہا ہے ..."

#: live-installer/installer.py:204
msgid "Adding new user to the system"
msgstr "سسٹم میں نیا صارف شامل کر رہا ہے"

#: live-installer/installer.py:255
msgid "Writing filesystem mount information to /etc/fstab"
msgstr ""

#: live-installer/installer.py:261 live-installer/installer.py:417
#: live-installer/installer.py:432
#, python-format
msgid "Mounting %(partition)s on %(mountpoint)s"
msgstr ""

#: live-installer/installer.py:298
#, python-format
msgid "Filling %s with random data (please be patient, this can take hours...)"
msgstr ""

#: live-installer/installer.py:304
#, python-format
msgid "Creating partitions on %s"
msgstr ""

#: live-installer/installer.py:369
#, python-format
msgid "Formatting %(partition)s as %(format)s ..."
msgstr ""

#: live-installer/installer.py:558
msgid "Setting hostname"
msgstr "ھوسٹ کا نام سیٹ کر رہا ہے"

#: live-installer/installer.py:583
msgid "Setting locale"
msgstr "لوکیل سیٹ کیا جا رہا ہے"

#: live-installer/installer.py:604
#, fuzzy
msgid "Setting timezone"
msgstr "ھوسٹ کا نام سیٹ کر رہا ہے"

#: live-installer/installer.py:637
msgid "Setting keyboard options"
msgstr "کیی بورڈ کے اختیارات سیٹ کیے جا رہے ہیں"

#: live-installer/installer.py:733
msgid "Trying to install updates"
msgstr ""

#: live-installer/installer.py:737
msgid "Clearing package manager"
msgstr ""

#: live-installer/installer.py:754
msgid "Generating initramfs"
msgstr ""

#: live-installer/installer.py:759
msgid "Preparing bootloader installation"
msgstr ""

#: live-installer/installer.py:771
msgid "Installing bootloader"
msgstr "بوٹ لوڈر نصب کیا جا رہا ہے"

#: live-installer/installer.py:783
msgid "Configuring bootloader"
msgstr "بوٹ لوڈر ترتیب دیا جا رہا ہے"

#: live-installer/installer.py:791
msgid ""
"WARNING: The grub bootloader was not configured properly! You need to "
"configure it manually."
msgstr ""
"انتباہ: گرب بوٹ لوڈر کو صحیح سے تشکیل نہیں دیا جا سکا۔ آپ کو اس کو دستی "
"طریقے سے تشکیل دینا ہو گا۔"

#: live-installer/installer.py:844
msgid "Checking bootloader"
msgstr "بوٹ لوڈر کو چیک کیا جا رہا ہے"

#: live-installer/installer.py:876
msgid "Failed to run command (Exited with {}):"
msgstr ""

#, fuzzy
#~ msgid "Select additional options"
#~ msgstr "کیی بورڈ کے اختیارات سیٹ کیے جا رہے ہیں"

#~ msgid "Removing live configuration (packages)"
#~ msgstr "والی ترتیب مٹا رہا ہے CD (packages)"

#~ msgid "Cleaning APT"
#~ msgstr "APT کو صاف کر رہا ہے"

#~ msgid "Advanced options"
#~ msgstr "اعلی اختیارات"

#~ msgid "Expert mode"
#~ msgstr "ماہر موڈ"

#~ msgid "Installation paused"
#~ msgstr "انسٹالیشن موقوف کردہ"
